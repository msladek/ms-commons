package ch.marcsladek.commons.hibernate.query;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import ch.marcsladek.commons.hibernate.Field;

public class DatabaseQueryTest {

  @Test
  public void test_null() {
    try {
      new DatabaseQuery(null, new HashSet<Field>(), 0);
      fail("should throw NPE");
    } catch (NullPointerException exc) {
      // expected
    }
    DatabaseQuery query = new DatabaseQuery("", null, 0);
    assertNotNull(query.getFields());
    assertEquals(0, query.getFields().size());
  }

  @Test
  public void test_immutablility() {
    Set<Field> fields = new HashSet<>();
    fields.add(TestField.asdf);
    DatabaseQuery query = new DatabaseQuery("", fields, 0);
    try {
      query.getFields().add(TestField.fdsa);
      fail("should throw UOE");
    } catch (UnsupportedOperationException exc) {
      // expected
    }
    assertEquals(1, query.getFields().size());
    fields.add(TestField.fdsa);
    assertEquals(2, fields.size());
    assertEquals(1, query.getFields().size());
  }

  @Test
  public void test_getBindedDatabaseQuery_empty() {
    Set<Field> fields = new HashSet<>();
    fields.add(TestField.asdf);
    fields.add(TestField.fdsa);
    DatabaseQuery query = new DatabaseQuery("this is a query string", fields, 0);
    try {
      query.getBindedDatabaseQuery();
      fail("should throw IAE");
    } catch (IllegalArgumentException exc) {
      // expected
    }
  }

  @Test
  public void test_getBindedDatabaseQuery_tooMany() {
    Set<Field> fields = new HashSet<>();
    fields.add(TestField.asdf);
    DatabaseQuery query = new DatabaseQuery("this is a query string", fields, 0);
    Map<Field, Object> binds = new HashMap<>();
    binds.put(TestField.asdf, "test1");
    binds.put(TestField.fdsa, "test2");
    try {
      query.getBindedDatabaseQuery(binds);
      fail("should throw IAE");
    } catch (IllegalArgumentException exc) {
      // expected
    }
  }

  @Test
  public void test_getBindedDatabaseQuery() {
    Set<Field> fields = new HashSet<>();
    fields.add(TestField.asdf);
    fields.add(TestField.fdsa);
    DatabaseQuery query = new DatabaseQuery("this is a query string", fields, 0);
    fields.clear(); // unmodifiable check
    Map<Field, Object> binds = new HashMap<>();
    binds.put(TestField.asdf, "test1");
    binds.put(TestField.fdsa, "test2");
    BindedDatabaseQuery bindedQuery = query.getBindedDatabaseQuery(binds);
    assertNotNull(bindedQuery);
    assertEquals("this is a query string", bindedQuery.getQueryString());
    assertEquals(binds.entrySet(), bindedQuery.getBinds());
  }

}
