package ch.marcsladek.commons.hibernate.query;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import ch.marcsladek.commons.hibernate.Field;

public class DatabaseQueryBuilderTest {

  @Test
  public void test_select() {
    DatabaseQueryBuilder builder = new DatabaseQueryBuilder(DatabaseQueryType.SELECT,
        TestField.class);
    DatabaseQuery query = builder.build();
    assertEquals("FROM TestField", query.getQueryString());
    assertEquals(0, query.getFields().size());
  }

  @Test
  public void test_count() {
    DatabaseQueryBuilder builder = new DatabaseQueryBuilder(DatabaseQueryType.COUNT,
        TestField.class);
    builder.setOrder(new ArrayList<Field>(), true); // shouldn't change query
    DatabaseQuery query = builder.build();
    assertEquals("SELECT COUNT(id) FROM TestField", query.getQueryString());
    assertEquals(0, query.getFields().size());
  }

  @Test
  public void test_field() {
    DatabaseQueryBuilder builder = new DatabaseQueryBuilder(DatabaseQueryType.SELECT,
        TestField.class);
    builder.addField(TestField.asdf);
    DatabaseQuery query = builder.build();
    assertEquals("FROM TestField WHERE asdf = :asdf", query.getQueryString());
    assertEquals(1, query.getFields().size());
    assertTrue(query.getFields().contains(TestField.asdf));
  }

  @Test
  public void test_field_2() {
    DatabaseQueryBuilder builder = new DatabaseQueryBuilder(DatabaseQueryType.SELECT,
        TestField.class);
    builder.addField(TestField.asdf);
    builder.addField(TestField.fdsa);
    DatabaseQuery query = builder.build();
    assertEquals("FROM TestField WHERE asdf = :asdf AND fdsa = :fdsa", query.getQueryString());
    assertEquals(2, query.getFields().size());
    assertTrue(query.getFields().contains(TestField.asdf));
    assertTrue(query.getFields().contains(TestField.fdsa));
  }

  @Test
  public void test_field_or() {
    DatabaseQueryBuilder builder = new DatabaseQueryBuilder(DatabaseQueryType.SELECT,
        TestField.class);
    builder.addOrFields(Arrays.asList(new Field[] { TestField.asdf, TestField.fdsa }));
    DatabaseQuery query = builder.build();
    assertEquals("FROM TestField WHERE asdf = :asdf OR fdsa = :fdsa", query.getQueryString());
    assertEquals(2, query.getFields().size());
    assertTrue(query.getFields().contains(TestField.asdf));
    assertTrue(query.getFields().contains(TestField.fdsa));
  }

  @Test
  public void test_field_or_2() {
    DatabaseQueryBuilder builder = new DatabaseQueryBuilder(DatabaseQueryType.SELECT,
        TestField.class);
    builder.addOrFields(Arrays.asList(new Field[] { TestField.asdf, TestField.fdsa }));
    builder.addOrFields(Arrays.asList(new Field[] { TestField.fdsa, TestField.asdf }));
    DatabaseQuery query = builder.build();
    assertEquals("FROM TestField WHERE (asdf = :asdf OR fdsa = :fdsa) "
        + "AND (fdsa = :fdsa OR asdf = :asdf)", query.getQueryString());
    assertEquals(2, query.getFields().size());
    assertTrue(query.getFields().contains(TestField.asdf));
    assertTrue(query.getFields().contains(TestField.fdsa));
  }

  @Test
  public void test_field_mixed() {
    DatabaseQueryBuilder builder = new DatabaseQueryBuilder(DatabaseQueryType.SELECT,
        TestField.class);
    builder.addField(TestField.asdf);
    builder.addOrFields(Arrays.asList(new Field[] { TestField.asdf, TestField.fdsa }));
    builder.addOrFields(Arrays.asList(new Field[] { TestField.fdsa, TestField.asdf }));
    DatabaseQuery query = builder.build();
    assertEquals("FROM TestField WHERE asdf = :asdf AND (asdf = :asdf OR fdsa = :fdsa) "
        + "AND (fdsa = :fdsa OR asdf = :asdf)", query.getQueryString());
    assertEquals(2, query.getFields().size());
    assertTrue(query.getFields().contains(TestField.asdf));
    assertTrue(query.getFields().contains(TestField.fdsa));
  }

  @Test
  public void test_field_order() {
    DatabaseQueryBuilder builder = new DatabaseQueryBuilder(DatabaseQueryType.SELECT,
        TestField.class);
    builder.addField(TestField.asdf);
    List<Field> order = new ArrayList<>();
    order.add(TestField.asdf);
    builder.setOrder(order, true);
    DatabaseQuery query = builder.build();
    assertEquals("FROM TestField WHERE asdf = :asdf ORDER BY asdf ASC", query.getQueryString());
    assertEquals(1, query.getFields().size());
    assertTrue(query.getFields().contains(TestField.asdf));
  }

  @Test
  public void test_field_order_2() {
    DatabaseQueryBuilder builder = new DatabaseQueryBuilder(DatabaseQueryType.SELECT,
        TestField.class);
    builder.addField(TestField.asdf);
    List<Field> order = new ArrayList<>();
    order.add(TestField.asdf);
    order.add(TestField.fdsa);
    builder.setOrder(order, false);
    DatabaseQuery query = builder.build();
    assertEquals("FROM TestField WHERE asdf = :asdf ORDER BY asdf, fdsa DESC",
        query.getQueryString());
    assertEquals(1, query.getFields().size());
    assertTrue(query.getFields().contains(TestField.asdf));
  }

  @Test
  public void test_field_order_revoke() {
    DatabaseQueryBuilder builder = new DatabaseQueryBuilder(DatabaseQueryType.SELECT,
        TestField.class);
    builder.addField(TestField.asdf);
    List<Field> order = new ArrayList<>();
    order.add(TestField.asdf);
    order.add(TestField.fdsa);
    builder.setOrder(order, false);
    builder.revokeOrder();
    DatabaseQuery query = builder.build();
    assertEquals("FROM TestField WHERE asdf = :asdf", query.getQueryString());
    assertEquals(1, query.getFields().size());
    assertTrue(query.getFields().contains(TestField.asdf));
  }

  @Test
  public void test_limit() {
    DatabaseQueryBuilder builder = new DatabaseQueryBuilder(DatabaseQueryType.SELECT,
        TestField.class);
    builder.addField(TestField.asdf);
    builder.setLimit(5);
    DatabaseQuery query = builder.build();
    assertEquals("FROM TestField WHERE asdf = :asdf", query.getQueryString());
    assertEquals(1, query.getFields().size());
    assertTrue(query.getFields().contains(TestField.asdf));
    assertEquals(5, query.getLimit());
  }

  @Test
  public void test_limit_illegal() {
    DatabaseQueryBuilder builder = new DatabaseQueryBuilder(DatabaseQueryType.SELECT,
        TestField.class);
    builder.setLimit(5);
    builder.setLimit(0);
    try {
      builder.setLimit(-1);
      fail("Should throw IAE");
    } catch (IllegalArgumentException exc) {
      // expected
    }
    DatabaseQuery query = builder.build();
    assertEquals(0, query.getLimit());
  }

}
