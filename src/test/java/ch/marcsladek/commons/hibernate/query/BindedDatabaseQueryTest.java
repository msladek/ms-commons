package ch.marcsladek.commons.hibernate.query;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.Test;

import ch.marcsladek.commons.hibernate.Field;

public class BindedDatabaseQueryTest {

  @Test
  public void test() {
    Map<Field, Object> binds = new HashMap<>();
    binds.put(TestField.asdf, "test1");
    binds.put(TestField.fdsa, "test2");
    DatabaseQuery query = new DatabaseQuery("this is a query string", binds.keySet(), 7);
    BindedDatabaseQuery bindedQuery = query.getBindedDatabaseQuery(binds);
    assertEquals("this is a query string", bindedQuery.getQueryString());
    Set<Entry<Field, Object>> bindSet = bindedQuery.getBinds();
    assertEquals(2, bindSet.size());
    for (Entry<Field, Object> entry : bindSet) {
      if (entry.getKey() == TestField.asdf) {
        assertEquals("test1", entry.getValue());
      } else if (entry.getKey() == TestField.fdsa) {
        assertEquals("test2", entry.getValue());
      } else {
        fail("invalid field");
      }
    }
  }

  @Test
  public void test_immutability() {
    Map<Field, Object> binds = new HashMap<>();
    binds.put(TestField.asdf, "test1");
    binds.put(TestField.fdsa, "test2");
    DatabaseQuery query = new DatabaseQuery("this is a query string", binds.keySet(), 7);
    BindedDatabaseQuery bindedQuery = query.getBindedDatabaseQuery(binds);
    try {
      bindedQuery.getBinds().remove(binds.entrySet().iterator().next());
      fail("should fail when modifying");
    } catch (UnsupportedOperationException exc) {
      // expected
    }
    assertEquals(2, bindedQuery.getBinds().size());
    binds.remove(TestField.asdf);
    assertEquals(1, binds.size());
    assertEquals(1, bindedQuery.getBinds().size());
  }

}
