package ch.marcsladek.commons.concurrent;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.same;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TimedManagerTest {

  TimedManager<Long, TimeManageable<Long>> manager;
  TimeManageable<Long> test;

  List<Integer> list;
  Set<Integer> checkSet;

  @Before
  @SuppressWarnings("unchecked")
  public void setUp() throws InterruptedException {
    test = createMock(TimeManageable.class);
    manager = new TimedManager<>(90, TimeUnit.MILLISECONDS);
    manager.start();
    Thread.sleep(10);
  }

  @Test
  public void test_enroll_disenroll() throws InterruptedException {

    expect(test.getIdentifier()).andReturn(4L).times(4);
    test.notifyDisenroll(same(manager));
    expectLastCall().once();

    replay(test);
    assertEquals(0, manager.getEnrolleeSize());
    assertTrue(manager.enroll(test));
    assertFalse(manager.enroll(test));
    assertEquals(1, manager.getEnrolleeSize());
    assertSame(test, manager.enrolleeMap.get(4L).getKey());
    assertEquals(new Long(4), manager.queue.peek());
    assertTrue(manager.disenroll(test));
    assertEquals(0, manager.getEnrolleeSize());
    assertEquals(new Long(4), manager.queue.peek());
    Thread.sleep(100);
    assertNull(manager.queue.peek());
    assertTrue(manager.isRunning());
    verify(test);
  }

  @Test
  public void test_enroll_autodisenroll() throws InterruptedException {

    expect(test.getIdentifier()).andReturn(4L).times(2);
    test.notifyDisenroll(same(manager));
    expectLastCall().once();

    replay(test);
    assertEquals(0, manager.getEnrolleeSize());
    assertTrue(manager.enroll(test));
    assertEquals(1, manager.getEnrolleeSize());
    assertSame(test, manager.enrolleeMap.get(4L).getKey());
    assertEquals(new Long(4), manager.queue.peek());
    assertTrue(manager.isRunning());
    Thread.sleep(100);
    assertEquals(0, manager.getEnrolleeSize());
    assertNull(manager.queue.peek());
    assertTrue(manager.isRunning());
    verify(test);
  }

  @Test
  public void test_shutdown() throws InterruptedException {

    expect(test.getIdentifier()).andReturn(4L).times(2);
    test.notifyDisenroll(same(manager));
    expectLastCall().once();

    replay(test);
    assertEquals(0, manager.getEnrolleeSize());
    assertTrue(manager.enroll(test));
    assertEquals(1, manager.getEnrolleeSize());
    assertSame(test, manager.enrolleeMap.get(4L).getKey());
    assertEquals(new Long(4), manager.queue.peek());
    assertTrue(manager.isRunning());
    manager.shutdown();
    Thread.sleep(10);
    assertFalse(manager.isRunning());
    assertEquals(0, manager.getEnrolleeSize());
    assertNull(manager.queue.peek());
    verify(test);
  }

  @After
  public void breakDown() {
    manager.shutdown();
  }

}
