package ch.marcsladek.commons.concurrent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;

import ch.marcsladek.commons.concurrent.CallDelayer;
import ch.marcsladek.commons.concurrent.Callable;

public class CallDelayerTest {

  private CallDelayer<String> callDelayer;
  private MyCallable myCallable;

  @Before
  public void setUp() {
    myCallable = new MyCallable();
    callDelayer = new CallDelayer<String>(myCallable, 100, TimeUnit.MILLISECONDS);
  }

  @Test
  public void testCall_none() throws InterruptedException {
    callDelayer.start();
    Thread.sleep(50);
    callDelayer.shutdown();
    List<List<String>> list = myCallable.getObjectsList();
    assertNotNull(list);
    assertEquals(0, myCallable.getObjectsList().size());
  }

  @Test
  public void testCall_once() throws InterruptedException {
    callDelayer.start();
    callDelayer.call("muh1");
    Thread.sleep(110);
    callDelayer.shutdown();

    List<List<String>> objectsList = myCallable.getObjectsList();
    assertNotNull(objectsList);
    assertEquals(1, objectsList.size());

    List<String> list1 = objectsList.get(0);
    assertNotNull(list1);
    assertEquals(1, list1.size());
    assertEquals("muh1", list1.get(0));
  }

  @Test
  public void testCall_multiple() throws InterruptedException {
    callDelayer.start();
    callDelayer.call("muh1");
    Thread.sleep(10);
    callDelayer.call("muh2");
    callDelayer.call("muh3");
    callDelayer.call("muh4");
    Thread.sleep(110);
    callDelayer.call("muh5");
    callDelayer.call("muh6");
    Thread.sleep(110);
    callDelayer.shutdown();

    List<List<String>> objectsList = myCallable.getObjectsList();
    assertNotNull(objectsList);
    assertEquals(3, objectsList.size());

    List<String> list1 = objectsList.get(0);
    assertNotNull(list1);
    assertEquals(1, list1.size());
    assertEquals("muh1", list1.get(0));

    List<String> list2 = objectsList.get(1);
    assertNotNull(list2);
    assertEquals(3, list2.size());
    assertEquals("muh2", list2.get(0));
    assertEquals("muh3", list2.get(1));
    assertEquals("muh4", list2.get(2));

    List<String> list3 = objectsList.get(2);
    assertNotNull(list3);
    assertEquals(2, list3.size());
    assertEquals("muh5", list3.get(0));
    assertEquals("muh6", list3.get(1));
  }

  @Test
  public void testCall_shutdown() throws InterruptedException {
    callDelayer.start();
    callDelayer.call("muh1");
    Thread.sleep(10);
    callDelayer.call("muh2");
    callDelayer.call("muh3");
    callDelayer.shutdown();
    Thread.sleep(110);

    List<List<String>> objectsList = myCallable.getObjectsList();
    assertNotNull(objectsList);
    assertEquals(1, objectsList.size());

    List<String> list1 = objectsList.get(0);
    assertNotNull(list1);
    assertEquals(1, list1.size());
    assertEquals("muh1", list1.get(0));
  }

  @Test
  public void testCall_ISE_notStarted_call() {
    try {
      callDelayer.call("muh1");
    } catch (IllegalStateException exc) {
      assertEquals("CallDelayer has not been started yet", exc.getMessage());
    }
  }

  @Test
  public void testCall_ISE_notStarted_shutdown() {
    try {
      callDelayer.shutdown();
    } catch (IllegalStateException exc) {
      assertEquals("CallDelayer has not been started yet", exc.getMessage());
    }
  }

  @Test
  public void testCall_ISE_alrdyStarted() {
    callDelayer.start();
    try {
      callDelayer.start();
    } catch (IllegalStateException exc) {
      assertEquals("CallDelayer has already been started once", exc.getMessage());
    }
    callDelayer.shutdown();
  }

}

class MyCallable implements Callable<Void, List<String>> {

  private final List<List<String>> objectsList;

  MyCallable() {
    objectsList = new ArrayList<>();
  }

  synchronized List<List<String>> getObjectsList() {
    return objectsList;
  }

  @Override
  public synchronized Void call(List<String> objects) {
    objectsList.add(objects);
    return null;
  }

}