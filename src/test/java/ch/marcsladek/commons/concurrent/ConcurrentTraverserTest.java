package ch.marcsladek.commons.concurrent;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ConcurrentTraverserTest {

  ConcurrentTraverser<Integer> concTraverser;
  int nb = 1000;

  List<Integer> list;
  Set<Integer> checkSet;

  @Before
  public void setUp() {
    checkSet = Collections.newSetFromMap(new ConcurrentHashMap<Integer, Boolean>());
    list = new ArrayList<>();
    for (int i = 0; i < 1000; i++) {
      list.add(i);
    }
    concTraverser = new ConcurrentTraverser<Integer>() {
      @Override
      protected void process(Integer element) {
        assertTrue(checkSet.add(element));
      }
    };
  }

  @Test
  public void test() throws InterruptedException, ExecutionException {
    checkSet.clear();
    concTraverser.traverse(list).awaitCompletion();
    assertEquals(nb, checkSet.size());

    checkSet.clear();
    concTraverser.traverse(list).awaitCompletion();
    assertEquals(nb, checkSet.size());
  }

  @After
  public void breakDown() {
    concTraverser.shutdown();
  }

}