package ch.marcsladek.commons.util.tree;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import ch.marcsladek.commons.util.tree.TreeNode;

/**
 * @author Marc Sladek
 */
public class TreeNodeTest {

  TreeNode<Integer> root;
  TreeNode<Integer> node1;
  TreeNode<Integer> node2;
  TreeNode<Integer> node3;
  TreeNode<Integer> node4;
  TreeNode<Integer> node5;
  TreeNode<Integer> node6;

  @Before
  public void setUp() {
    root = new TreeNode<Integer>("root", 1);
    node1 = new TreeNode<Integer>("node1", 5);
    node2 = new TreeNode<Integer>("node2", 10);
    node3 = new TreeNode<Integer>("node3", 3);
    node4 = new TreeNode<Integer>("node4", 8);
    node5 = new TreeNode<Integer>("node5", 4);
    node6 = new TreeNode<Integer>("node6", 33);
  }

  @Test
  public void testAddChild_getChild() {
    root.addChild(node1);
    root.addChild(node2);
    root.addChild(node3);
    node1.addChild(node4);
    node2.addChild(node5);
    node4.addChild(node6);

    assertEquals(3, root.getChildrenSize());
    assertSame(node1, root.getChild(0));
    assertSame(node2, root.getChild(1));
    assertSame(node3, root.getChild(2));
    assertEquals(1, node1.getChildrenSize());
    assertSame(node4, node1.getChild(0));
    assertEquals(1, node2.getChildrenSize());
    assertSame(node5, node2.getChild(0));
    assertEquals(1, node4.getChildrenSize());
    assertSame(node6, node4.getChild(0));
  }

  @Test
  public void testAddChild_IllegalArgumentException() {
    root.addChild(node1);
    try {
      root.addChild(node1);
      fail("Should have thrown Exception");
    } catch (IllegalArgumentException iae) {
      assertEquals("Cannot add 'TreeNode node1, 5' as child to 'TreeNode root, 1', it "
          + "already has parent TreeNode root, 1'", iae.getMessage());
    }
  }

  @Test
  public void testGetPath() {
    root.addChild(node1);
    root.addChild(node2);
    root.addChild(node3);
    node1.addChild(node4);
    node2.addChild(node5);
    node4.addChild(node6);
    assertEquals("root", root.getPath());
    assertEquals("root/node1", node1.getPath());
    assertEquals("root/node2", node2.getPath());
    assertEquals("root/node3", node3.getPath());
    assertEquals("root/node1/node4", node4.getPath());
    assertEquals("root/node2/node5", node5.getPath());
    assertEquals("root/node1/node4/node6", node6.getPath());
  }

  @Test
  public void testIsRoot() {
    root.addChild(node1);
    assertTrue(root.isRoot());
    assertFalse(node1.isRoot());
  }

  @Test
  public void testAddSubChildIndexes() {
    root.addSubChild((int[]) null, node1);
    root.addSubChild(new int[0], node2);
    root.addSubChild(new int[] {}, node3);
    root.addSubChild(new int[] { 1 }, node4);
    root.addSubChild(new int[] { 2 }, node5);
    root.addSubChild(new int[] { 1, 0 }, node6);

    assertEquals(3, root.getChildrenSize());
    assertSame(node1, root.getChild(0));
    assertSame(node2, root.getChild(1));
    assertSame(node3, root.getChild(2));
    assertEquals(1, node2.getChildrenSize());
    assertSame(node4, node2.getChild(0));
    assertEquals(1, node3.getChildrenSize());
    assertSame(node5, node3.getChild(0));
    assertEquals(1, node4.getChildrenSize());
    assertSame(node6, node4.getChild(0));
  }

  @Test
  public void testGetChildIndex_IndexOutOfBounds() {
    root.addChild(node1);
    root.addChild(node2);
    try {
      root.getChild(2);
      fail("Should have thrown Exception");
    } catch (IndexOutOfBoundsException e) {
      assertEquals("Index: 2, Size: 2", e.getMessage());
    }
  }

  @Test
  public void testRemoveChildIndex() {
    root.addChild(node1);
    root.addChild(node2);
    root.addChild(node3);
    assertEquals(3, root.getChildrenSize());
    root.removeChild(2);
    assertEquals(2, root.getChildrenSize());
    assertSame(node1, root.getChild(0));
    assertSame(node2, root.getChild(1));
    root.removeChild(0);
    assertEquals(1, root.getChildrenSize());
    assertSame(node2, root.getChild(0));
    root.removeChild(0);
    assertEquals(0, root.getChildrenSize());
  }

  @Test
  public void testRemoveChildIndex_IndexOutOfBounds() {
    root.addChild(node1);
    root.addChild(node2);
    try {
      root.removeChild(2);
      fail("Should have thrown Exception");
    } catch (IndexOutOfBoundsException e) {
      assertEquals("Index: 2, Size: 2", e.getMessage());
    }
  }

  @Test
  public void testGetChildrenSize() {
    root.addChild(node1);
    root.addChild(node2);
    root.addChild(node3);
    node3.addChild(root);
    assertEquals(3, root.getChildrenSize());
  }

  // 31 * x + hc
  @Test
  public void testHashCode() {
    int hashRoot = (31 * ((31 * 1) + root.getElement())) + 0;
    int hashNode1 = (31 * ((31 * 1) + node1.getElement())) + 0;
    assertEquals(hashRoot, root.hashCode());
    assertEquals(hashNode1, node1.hashCode());

    root.addChild(node1);
    hashRoot = (31 * ((31 * ((31 * 1) + root.getElement())) + 0)) + node1.getElement();
    hashNode1 = (31 * ((31 * 1) + node1.getElement())) + root.getElement();
    assertEquals(hashRoot, root.hashCode());
    assertEquals(hashNode1, node1.hashCode());

    node1.addChild(node2);
    hashNode1 = (31 * ((31 * ((31 * 1) + node1.getElement())) + root.getElement()))
        + node2.getElement();
    assertEquals(hashRoot, root.hashCode());
    assertEquals(hashNode1, node1.hashCode());
  }

  @Test
  public void testEquals() {
    assertEquals(root, new TreeNode<Integer>("", 1));
    root.addChild(node1);
    node1.addChild(node2);
    node1.addChild(node3);
    TreeNode<Integer> node = new TreeNode<Integer>("", 5);
    new TreeNode<Integer>("", 1).addChild(node);
    node.addChild(new TreeNode<Integer>("", 10));
    node.addChild(new TreeNode<Integer>("", 3));
    assertEquals(node1, node);
  }
}
