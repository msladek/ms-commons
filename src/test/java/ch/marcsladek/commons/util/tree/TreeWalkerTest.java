package ch.marcsladek.commons.util.tree;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Marc Sladek
 */
public class TreeWalkerTest {

  TreeWalker<Integer> treeWalker;

  TreeNode<Integer> root;
  TreeNode<Integer> node1;
  TreeNode<Integer> node2;
  TreeNode<Integer> node3;
  TreeNode<Integer> node4;
  TreeNode<Integer> node5;
  TreeNode<Integer> node6;

  @Before
  public void setUp() {
    treeWalker = new TreeWalker<Integer>();
    root = new TreeNode<Integer>("root", 1);
    node1 = new TreeNode<Integer>("node1", 5);
    node2 = new TreeNode<Integer>("node2", 10);
    node3 = new TreeNode<Integer>("node3", 3);
    node4 = new TreeNode<Integer>("node4", 8);
    node5 = new TreeNode<Integer>("node5", 4);
    node6 = new TreeNode<Integer>("node6", 33);
  }

  @Test
  public void testWalk_empty() {
    boolean result = treeWalker.walk(root, new TreeVisitor<Integer>() {

      @Override
      public TreeVisitResult preVisitNode(Integer element) {
        assertEquals(Integer.valueOf(1), element);
        return element == 1 ? TreeVisitResult.CONTINUE : TreeVisitResult.TERMINATE;
      }

      @Override
      public TreeVisitResult postVisitNode(Integer element) {
        assertEquals(Integer.valueOf(1), element);
        return element == 1 ? TreeVisitResult.CONTINUE : TreeVisitResult.TERMINATE;
      }
    });

    assertTrue(result);
  }

  @Test
  public void testWalk() {
    root.addChild(node1);
    root.addChild(node2);
    root.addChild(node3);
    node1.addChild(node4);
    node2.addChild(node5);
    node4.addChild(node6);

    boolean result = treeWalker.walk(root, new TreeVisitor<Integer>() {

      int[] elementsPre = { 1, 5, 8, 33, 10, 4, 3 };
      int indexPre = 0;
      int[] elementsPost = { 33, 8, 5, 4, 10, 3, 1 };
      int indexPost = 0;

      @Override
      public TreeVisitResult preVisitNode(Integer element) {
        Integer i = elementsPre[indexPre++];
        assertEquals(i, element);
        return element == i ? TreeVisitResult.CONTINUE : TreeVisitResult.TERMINATE;
      }

      @Override
      public TreeVisitResult postVisitNode(Integer element) {
        Integer i = elementsPost[indexPost++];
        assertEquals(i, element);
        return element == i ? TreeVisitResult.CONTINUE : TreeVisitResult.TERMINATE;
      }
    });

    assertTrue(result);
  }

  @Test
  public void testWalk_skip() {
    root.addChild(node1);
    root.addChild(node2);
    root.addChild(node3);
    node1.addChild(node4);
    node2.addChild(node5);
    node4.addChild(node6);

    boolean result = treeWalker.walk(root, new TreeVisitor<Integer>() {

      int[] elementsPre = { 1, 5, 10, 4, 3 };
      int indexPre = 0;
      int[] elementsPost = { 5, 4, 10, 3, 1 };
      int indexPost = 0;

      @Override
      public TreeVisitResult preVisitNode(Integer element) {
        Integer i = elementsPre[indexPre++];
        assertEquals(i, element);
        if (i == 5) {
          return TreeVisitResult.SKIP_CHILDREN;
        } else {
          return element == i ? TreeVisitResult.CONTINUE : TreeVisitResult.TERMINATE;
        }
      }

      @Override
      public TreeVisitResult postVisitNode(Integer element) {
        Integer i = elementsPost[indexPost++];
        assertEquals(i, element);
        return element == i ? TreeVisitResult.CONTINUE : TreeVisitResult.TERMINATE;
      }
    });

    assertTrue(result);
  }

  @Test
  public void testWalk_terminate() {
    root.addChild(node1);
    root.addChild(node2);
    root.addChild(node3);
    node1.addChild(node4);
    node2.addChild(node5);
    node4.addChild(node6);

    boolean result = treeWalker.walk(root, new TreeVisitor<Integer>() {

      int[] elementsPre = { 1, 5, 8, 33, 10, 4 };
      int indexPre = 0;
      int[] elementsPost = { 33, 8, 5 };
      int indexPost = 0;

      @Override
      public TreeVisitResult preVisitNode(Integer element) {
        Integer i = elementsPre[indexPre++];
        assertEquals(i, element);
        if (i == 4) {
          return TreeVisitResult.TERMINATE;
        } else {
          return element == i ? TreeVisitResult.CONTINUE : TreeVisitResult.TERMINATE;
        }
      }

      @Override
      public TreeVisitResult postVisitNode(Integer element) {
        Integer i = elementsPost[indexPost++];
        assertEquals(i, element);
        return element == i ? TreeVisitResult.CONTINUE : TreeVisitResult.TERMINATE;
      }
    });

    assertFalse(result);
  }

  @Test
  public void testWalk_NPE_args() {
    try {
      treeWalker.walk(null, null);
      assertFalse(true);
    } catch (NullPointerException exc) {
      assertEquals("Given TreeNode is null", exc.getMessage());
    }
    try {
      treeWalker.walk(root, null);
      assertFalse(true);
    } catch (NullPointerException exc) {
      assertEquals("Given TreeVisitor is null", exc.getMessage());
    }
  }

  @Test
  public void testWalk_NPE_ret() {
    try {
      treeWalker.walk(root, new TreeVisitor<Integer>() {

        @Override
        public TreeVisitResult preVisitNode(Integer element) {
          return null;
        }

        @Override
        public TreeVisitResult postVisitNode(Integer element) {
          return null;
        }
      });
      assertFalse(true);
    } catch (NullPointerException exc) {
      assertEquals("TreeVisitor returned null", exc.getMessage());
    }
  }
}
