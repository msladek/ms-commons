package ch.marcsladek.commons.util.list;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MapBackedListTest {

  private static final String[] SAMPLE = new String[] { "A", "Lone", "Pale", "Blue", "Dot" };

  MapBackedList<String> list;

  @Before
  public void setUp() {
    list = new MapBackedList<>();
    for (String s : SAMPLE) {
      list.add(s);
    }
  }

  @Test
  public void testContains() {
    for (String s : SAMPLE) {
      assertTrue(list.contains(s));
    }
    assertFalse(list.contains("B"));
    assertFalse(list.contains("Hot"));
  }

  @Test
  public void testIndexOf() {
    for (int i = 0; i < SAMPLE.length; i++) {
      assertEquals(i, list.indexOf(SAMPLE[i]));
    }
  }

  @Test
  public void testAdd() {
    assertEquals(5, list.size());
    list.add("Blue");
    assertEquals(5, list.size());
    assertEquals("A", list.get(0));
    assertEquals(new Integer(0), list.getMap().get("A"));
    assertEquals("Lone", list.get(1));
    assertEquals(new Integer(1), list.getMap().get("Lone"));
    assertEquals("Pale", list.get(2));
    assertEquals(new Integer(2), list.getMap().get("Pale"));
    assertEquals("Blue", list.get(3));
    assertEquals(new Integer(3), list.getMap().get("Blue"));
    assertEquals("Dot", list.get(4));
    assertEquals(new Integer(4), list.getMap().get("Dot"));
  }

  @Test
  public void testRemove_index() {
    assertEquals(5, list.size());
    list.remove(3);
    assertEquals(4, list.size());
    assertEquals("A", list.get(0));
    assertEquals(new Integer(0), list.getMap().get("A"));
    assertEquals("Lone", list.get(1));
    assertEquals(new Integer(1), list.getMap().get("Lone"));
    assertEquals("Pale", list.get(2));
    assertEquals(new Integer(2), list.getMap().get("Pale"));
    assertEquals("Dot", list.get(3));
    assertEquals(new Integer(3), list.getMap().get("Dot"));
    list.remove(0);
    assertEquals(3, list.size());
    assertEquals("Lone", list.get(0));
    assertEquals(new Integer(0), list.getMap().get("Lone"));
    assertEquals("Pale", list.get(1));
    assertEquals(new Integer(1), list.getMap().get("Pale"));
    assertEquals("Dot", list.get(2));
    assertEquals(new Integer(2), list.getMap().get("Dot"));
    list.remove(1);
    assertEquals(2, list.size());
    assertEquals("Lone", list.get(0));
    assertEquals(new Integer(0), list.getMap().get("Lone"));
    assertEquals("Dot", list.get(1));
    assertEquals(new Integer(1), list.getMap().get("Dot"));
    list.remove(1);
    assertEquals(1, list.size());
    assertEquals("Lone", list.get(0));
    assertEquals(new Integer(0), list.getMap().get("Lone"));
    list.remove(0);
    assertEquals(0, list.size());
  }

  @Test
  public void testRemove_object() {
    assertEquals(5, list.size());
    list.remove("Blue");
    assertEquals(4, list.size());
    assertEquals("A", list.get(0));
    assertEquals(new Integer(0), list.getMap().get("A"));
    assertEquals("Lone", list.get(1));
    assertEquals(new Integer(1), list.getMap().get("Lone"));
    assertEquals("Pale", list.get(2));
    assertEquals(new Integer(2), list.getMap().get("Pale"));
    assertEquals("Dot", list.get(3));
    assertEquals(new Integer(3), list.getMap().get("Dot"));
    list.remove("A");
    assertEquals(3, list.size());
    assertEquals("Lone", list.get(0));
    assertEquals(new Integer(0), list.getMap().get("Lone"));
    assertEquals("Pale", list.get(1));
    assertEquals(new Integer(1), list.getMap().get("Pale"));
    assertEquals("Dot", list.get(2));
    assertEquals(new Integer(2), list.getMap().get("Dot"));
    list.remove("Pale");
    assertEquals(2, list.size());
    assertEquals("Lone", list.get(0));
    assertEquals(new Integer(0), list.getMap().get("Lone"));
    assertEquals("Dot", list.get(1));
    assertEquals(new Integer(1), list.getMap().get("Dot"));
    list.remove("Dot");
    assertEquals(1, list.size());
    assertEquals("Lone", list.get(0));
    assertEquals(new Integer(0), list.getMap().get("Lone"));
    list.remove("Lone");
    assertEquals(0, list.size());
  }

}
