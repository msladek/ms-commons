package ch.marcsladek.commons.math.vec;

public class VecInt3DInp extends AVecInt3D {

  private static final long serialVersionUID = 201301130352L;

  public VecInt3DInp(IVecInt vec) {
    super(vec);
  }

  public VecInt3DInp(int x, int y) {
    super(x, y);
  }

  public VecInt3DInp(int x, int y, int z) {
    super(x, y, z);
  }

  @Override
  public IVecInt add(IVecInt vec) {
    x += vec.getX();
    y += vec.getY();
    z += vec.getZ();
    return this;
  }

  @Override
  public IVecInt sub(IVecInt vec) {
    x -= vec.getX();
    y -= vec.getY();
    z -= vec.getZ();
    return this;
  }

  @Override
  public IVecInt mult(float s) {
    x = Math.round(x * s);
    y = Math.round(y * s);
    z = Math.round(z * s);
    return this;
  }

  @Override
  public IVecInt div(float s) {
    x = Math.round(x / s);
    y = Math.round(y / s);
    z = Math.round(z / s);
    return this;
  }

  @Override
  public IVecInt crossP(IVecInt vec) {
    int temp_x = x;
    int temp_y = y;
    x = (this.y * vec.getZ()) - (this.z * vec.getY());
    y = (this.z * vec.getX()) - (temp_x * vec.getZ());
    z = (temp_x * vec.getY()) - (temp_y * vec.getX());
    return this;
  }

  @Override
  public IVecInt invert() {
    int tmp = x;
    x = y;
    y = tmp;
    return this;
  }

  @Override
  public IVecInt invertXZ() {
    int tmp = x;
    x = z;
    z = tmp;
    return this;
  }

  @Override
  public IVecInt invertYZ() {
    int tmp = y;
    y = z;
    z = tmp;
    return this;
  }

  @Override
  public IVecInt negateX() {
    x *= -1;
    return this;
  }

  @Override
  public IVecInt negateY() {
    y *= -1;
    return this;
  }

  @Override
  public IVecInt negateZ() {
    z *= -1;
    return this;
  }

  @Override
  public IVecInt normalize() {
    return this.div(this.norm());
  }

  public static IVecInt parseVec(String s) throws NumberFormatException {
    s = s.replace("[", "").replace("]", "");
    String[] sa = s.split(" ");
    if (sa.length < 3) {
      sa = s.split(",");
    }
    if (sa.length < 3) {
      sa = s.split("x");
    }
    if (sa.length < 3) {
      sa = s.split("/");
    }
    for (String si : sa) {
      si = si.trim();
    }
    if (sa.length == 3) {
      return new VecInt3DInp(Integer.parseInt(sa[0]), Integer.parseInt(sa[1]),
          Integer.parseInt(sa[2]));
    } else {
      throw new NumberFormatException("Unable to parse '" + s + "' to Vec3DInt");
    }
  }
}