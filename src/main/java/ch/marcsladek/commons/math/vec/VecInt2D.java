package ch.marcsladek.commons.math.vec;

public class VecInt2D extends AVecInt2D {

  private static final long serialVersionUID = 201301130231L;

  public VecInt2D(IVecInt vec) {
    super(vec);
  }

  public VecInt2D(int x, int y) {
    super(x, y);
  }

  @Override
  public IVecInt add(IVecInt vec) {
    return new VecInt2D(this.x + vec.getX(), this.y + vec.getY());
  }

  @Override
  public IVecInt sub(IVecInt vec) {
    return new VecInt2D(this.x - vec.getX(), this.y - vec.getY());
  }

  @Override
  public IVecInt mult(float s) {
    return new VecInt2D(Math.round(this.x * s), Math.round(this.y * s));
  }

  @Override
  public IVecInt div(float s) {
    return new VecInt2D(Math.round(this.x / s), Math.round(this.y / s));
  }

  @Override
  public IVecInt invert() {
    return new VecInt2D(y, x);
  }

  @Override
  public IVecInt negateX() {
    return new VecInt2D(-x, y);
  }

  @Override
  public IVecInt negateY() {
    return new VecInt2D(x, -y);
  }

  @Override
  public IVecInt negateZ() {
    return new VecInt2D(this);
  }

  @Override
  public IVecInt normalize() {
    return this.div(this.norm());
  }

  public static IVecInt parseVec(String s) throws NumberFormatException {
    s = s.replace("[", "").replace("]", "");
    String[] sa = s.split(" ");
    if (sa.length < 2) {
      sa = s.split(",");
    }
    if (sa.length < 2) {
      sa = s.split("x");
    }
    if (sa.length < 2) {
      sa = s.split("/");
    }
    for (String si : sa) {
      si = si.trim();
    }
    if (sa.length == 2) {
      return new VecInt2D(Integer.parseInt(sa[0]), Integer.parseInt(sa[1]));
    } else {
      throw new NumberFormatException("Unable to parse '" + s + "' to VecInt2D");
    }
  }

}