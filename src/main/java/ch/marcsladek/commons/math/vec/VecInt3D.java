package ch.marcsladek.commons.math.vec;

public class VecInt3D extends AVecInt3D {

  private static final long serialVersionUID = 201301130352L;

  public VecInt3D(IVecInt vec) {
    super(vec);
  }

  public VecInt3D(int x, int y) {
    super(x, y);
  }

  public VecInt3D(int x, int y, int z) {
    super(x, y, z);
  }

  @Override
  public IVecInt add(IVecInt vec) {
    return new VecInt3D(this.x + vec.getX(), this.y + vec.getY(), this.z + vec.getZ());
  }

  @Override
  public IVecInt sub(IVecInt vec) {
    return new VecInt3D(this.x - vec.getX(), this.y - vec.getY(), this.z - vec.getZ());
  }

  @Override
  public IVecInt mult(float s) {
    return new VecInt3D(Math.round(x * s), Math.round(y * s), Math.round(z * s));
  }

  @Override
  public IVecInt div(float s) {
    return new VecInt3D(Math.round(x / s), Math.round(y / s), Math.round(z / s));
  }

  @Override
  public IVecInt crossP(IVecInt vec) {
    return new VecInt3D((this.y * vec.getZ()) - (this.z * vec.getY()),
        (this.z * vec.getX()) - (this.x * vec.getZ()), (this.x * vec.getY())
            - (this.y * vec.getX()));
  }

  @Override
  public IVecInt invert() {
    return new VecInt3D(y, x, z);
  }

  @Override
  public IVecInt invertXZ() {
    return new VecInt3D(z, y, x);
  }

  @Override
  public IVecInt invertYZ() {
    return new VecInt3D(x, z, y);
  }

  @Override
  public IVecInt negateX() {
    return new VecInt3D(-x, y, z);
  }

  @Override
  public IVecInt negateY() {
    return new VecInt3D(x, -y, z);
  }

  @Override
  public IVecInt negateZ() {
    return new VecInt3D(x, y, -z);
  }

  @Override
  public IVecInt normalize() {
    return this.div(this.norm());
  }

  public static IVecInt parseVec(String s) throws NumberFormatException {
    s = s.replace("[", "").replace("]", "");
    String[] sa = s.split(" ");
    if (sa.length < 3) {
      sa = s.split(",");
    }
    if (sa.length < 3) {
      sa = s.split("x");
    }
    if (sa.length < 3) {
      sa = s.split("/");
    }
    for (String si : sa) {
      si = si.trim();
    }
    if (sa.length == 3) {
      return new VecInt3D(Integer.parseInt(sa[0]), Integer.parseInt(sa[1]),
          Integer.parseInt(sa[2]));
    } else {
      throw new NumberFormatException("Unable to parse '" + s + "' to Vec3DInt");
    }
  }
}