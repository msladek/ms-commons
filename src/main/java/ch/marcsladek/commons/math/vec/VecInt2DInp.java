package ch.marcsladek.commons.math.vec;

public class VecInt2DInp extends AVecInt2D {

  private static final long serialVersionUID = 201301130100L;

  public VecInt2DInp(IVecInt vec) {
    super(vec);
  }

  public VecInt2DInp(int x, int y) {
    super(x, y);
  }

  @Override
  public IVecInt add(IVecInt vec) {
    x += vec.getX();
    y += vec.getY();
    return this;
  }

  @Override
  public IVecInt sub(IVecInt vec) {
    x -= vec.getX();
    y -= vec.getY();
    return this;
  }

  @Override
  public IVecInt mult(float s) {
    x = Math.round(x * s);
    y = Math.round(y * s);
    return this;
  }

  @Override
  public IVecInt div(float s) {
    x = Math.round(this.x / s);
    y = Math.round(this.y / s);
    return this;
  }

  @Override
  public IVecInt invert() {
    int tmp = x;
    x = y;
    y = tmp;
    return this;
  }

  @Override
  public IVecInt negateX() {
    x *= -1;
    return this;
  }

  @Override
  public IVecInt negateY() {
    y *= -1;
    return this;
  }

  @Override
  public IVecInt negateZ() {
    return this;
  }

  @Override
  public IVecInt normalize() {
    return this.div(this.norm());
  }

  public static IVecInt parseVec(String s) throws NumberFormatException {
    s = s.replace("[", "").replace("]", "");
    String[] sa = s.split(" ");
    if (sa.length < 2) {
      sa = s.split(",");
    }
    if (sa.length < 2) {
      sa = s.split("x");
    }
    if (sa.length < 2) {
      sa = s.split("/");
    }
    for (String si : sa) {
      si = si.trim();
    }
    if (sa.length == 2) {
      return new VecInt2DInp(Integer.parseInt(sa[0]), Integer.parseInt(sa[1]));
    } else {
      throw new NumberFormatException("Unable to parse '" + s + "' to VecInt");
    }
  }
}