package ch.marcsladek.commons.math.vec;

import java.awt.Point;
import java.awt.Rectangle;

public abstract class AVecInt2D implements IVecInt {

  private static final long serialVersionUID = 201301130332L;

  public static final IVecInt ZERO = new VecInt2D(0, 0);
  public static final IVecInt ONE = new VecInt2D(1, 1);

  protected int x;
  protected int y;

  protected AVecInt2D(IVecInt vec) {
    this.x = vec.getX();
    this.y = vec.getY();
  }

  protected AVecInt2D(int x, int y) {
    this.x = x;
    this.y = y;
  }

  @Override
  public final int getX() {
    return x;
  }

  @Override
  public final int getY() {
    return y;
  }

  @Override
  public final int getZ() {
    return 0;
  }

  @Override
  public IVecInt crossP(IVecInt vec) {
    return new VecInt3D(this.y * vec.getZ(), 0 - (this.x * vec.getZ()),
        (this.x * vec.getY()) - (this.y * vec.getX()));
  }

  @Override
  public final long dotP(IVecInt vec) {
    return ((long) this.x * vec.getX()) + ((long) this.y * vec.getY());
  }

  @Override
  public final float norm() {
    return (float) Math.sqrt(this.dotP(this));
  }

  @Override
  public final long norm_sqr() {
    return this.dotP(this);
  }

  @Override
  public final float slope() {
    return slope(x, y);
  }

  @Override
  public final float slopeXZ() {
    return slope(x, 0);
  }

  @Override
  public final float slopeYZ() {
    return slope(y, 0);
  }

  private float slope(int a, int b) {
    if (a != 0) {
      return b / a;
    } else {
      return Float.NaN;
    }
  }

  @Override
  public IVecInt invertXZ() {
    return new VecInt3D(0, y, x);
  }

  @Override
  public IVecInt invertYZ() {
    return new VecInt3D(x, 0, y);
  }

  @Override
  public final boolean isInDistanceFrom(IVecInt v, long distance) {
    long dx = x - v.getX();
    long dy = y - v.getY();
    return ((dx * dx) + (dy * dy)) <= (distance * distance);
  }

  public final Point toPoint() {
    return new Point(x, y);
  }

  public final Rectangle toRect(IVecInt vec) {
    return new Rectangle(x, y, vec.getX(), vec.getY());
  }

  @Override
  public int hashCode() {
    int prime = 31;
    int result = 1;
    result = (prime * result) + x;
    result = (prime * result) + y;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj instanceof IVecInt) {
      IVecInt other = (IVecInt) obj;
      return (x == other.getX()) && (y == other.getY());
    } else {
      return false;
    }
  }

  @Override
  public String toString() {
    return "[" + x + " " + y + "]";
  }
  
}
