package ch.marcsladek.commons.math.vec;

import java.io.Serializable;

public interface IVecInt extends Serializable {

  public int getX();

  public int getY();

  public int getZ();

  public IVecInt add(IVecInt vec);

  public IVecInt sub(IVecInt vec);

  public IVecInt mult(float s);

  public IVecInt div(float s);

  public long dotP(IVecInt vec);

  public IVecInt crossP(IVecInt vec);

  public float norm();

  public long norm_sqr();

  public float slope();

  public float slopeXZ();

  public float slopeYZ();

  public IVecInt invert();

  public IVecInt invertXZ();

  public IVecInt invertYZ();

  public IVecInt negateX();

  public IVecInt negateY();

  public IVecInt negateZ();

  public IVecInt normalize();

  public boolean isInDistanceFrom(IVecInt v, long distance);

}