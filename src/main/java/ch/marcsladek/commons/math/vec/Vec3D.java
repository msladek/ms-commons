package ch.marcsladek.commons.math.vec;

import java.io.Serializable;

public final class Vec3D implements Serializable {

  private static final long serialVersionUID = 201306061915L;

  public static final Vec3D ZERO = new Vec3D(0, 0, 0);
  public static final Vec3D ONE = new Vec3D(1, 1, 1);

  private double x;
  private double y;
  private double z;

  public Vec3D(Vec3D vec) {
    this.x = vec.x;
    this.y = vec.y;
    this.z = vec.z;
  }

  public Vec3D(double x, double y, double z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  public double getX() {
    return x;
  }

  public double getY() {
    return y;
  }

  public double getZ() {
    return z;
  }

  public int getXInt() {
    return (int) Math.round(x);
  }

  public int getYInt() {
    return (int) Math.round(y);
  }

  public int getZInt() {
    return (int) Math.round(z);
  }

  public Vec3D copy() {
    return new Vec3D(this);
  }

  public Vec3D add(Vec3D vec) {
    x += vec.x;
    y += vec.y;
    z += vec.z;
    return this;
  }

  public Vec3D sub(Vec3D vec) {
    x -= vec.x;
    y -= vec.y;
    z -= vec.z;
    return this;
  }

  public Vec3D mult(double s) {
    x *= s;
    y *= s;
    z *= s;
    return this;
  }

  public Vec3D div(double s) {
    x /= s;
    y /= s;
    z /= s;
    return this;
  }

  public double dotP(Vec3D vec) {
    return (x * vec.x) + (y * vec.y) + (z * vec.z);
  }

  public Vec3D crossP(Vec3D vec) {
    double temp_x = x;
    double temp_y = y;
    x = (this.y * vec.z) - (this.z * vec.y);
    y = (this.z * vec.x) - (temp_x * vec.z);
    z = (temp_x * vec.y) - (temp_y * vec.x);
    return this;
  }

  public double norm() {
    return Math.sqrt(this.dotP(this));
  }

  public double normSqr() {
    return this.dotP(this);
  }

  public double slopeXY() {
    return y / x;
  }

  public double slopeXZ() {
    return z / x;
  }

  public double slopeYZ() {
    return z / y;
  }

  public Vec3D invertXY() {
    double tmp = x;
    x = y;
    y = tmp;
    return this;
  }

  public Vec3D invertXZ() {
    double tmp = x;
    x = z;
    z = tmp;
    return this;
  }

  public Vec3D invertYZ() {
    double tmp = y;
    y = z;
    z = tmp;
    return this;
  }

  public Vec3D negateX() {
    x *= -1;
    return this;
  }

  public Vec3D negateY() {
    y *= -1;
    return this;
  }

  public Vec3D negateZ() {
    z *= -1;
    return this;
  }

  public Vec3D negate() {
    return this.negateX().negateY().negateZ();
  }

  public Vec3D absX() {
    x = Math.abs(x);
    return this;
  }

  public Vec3D absY() {
    y = Math.abs(y);
    return this;
  }

  public Vec3D absZ() {
    z = Math.abs(z);
    return this;
  }

  public Vec3D abs() {
    return this.absX().absY().absZ();
  }

  public Vec3D normalise() {
    return this.div(this.norm());
  }

  public double angle(Vec3D vec) {
    return Math.acos(this.dotP(vec) / (this.norm() * vec.norm()));
  }

  public double distance(Vec3D vec) {
    return Math.sqrt(this.distanceSqr(vec));
  }

  public double distanceSqr(Vec3D vec) {
    double dx = x - vec.x;
    double dy = y - vec.y;
    double dz = z - vec.z;
    return (dx * dx) + (dy * dy) + (dz * dz);
  }

  public boolean isInDistanceTo(Vec3D vec, double distance) {
    return this.distanceSqr(vec) <= (distance * distance);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    } else if (obj instanceof Vec3D) {
      return equals((Vec3D) obj, 0);
    } else {
      return false;
    }
  }

  public boolean equalsInt(Vec3D vec) {
    return (Math.round(x) == Math.round(vec.x))
        && ((Math.round(y) == Math.round(vec.y)) && (Math.round(z) == Math.round(vec.z)));
  }

  public boolean equals(Vec3D vec, int decimalPlace) {
    return (round(x, decimalPlace) == round(vec.x, decimalPlace))
        && (round(y, decimalPlace) == round(vec.y, decimalPlace))
        && (round(z, decimalPlace) == round(vec.z, decimalPlace));
  }

  private double round(double val, int decimalPlace) {
    if (decimalPlace > 0) {
      int val_int = (int) val;
      double nb = Math.pow(10, decimalPlace);
      return val_int + (Math.round((val - val_int) * nb) / nb);
    } else {
      return val;
    }
  }

  @Override
  public int hashCode() {
    int hashCode = 17;
    hashCode = (37 * hashCode) + new Float(x).hashCode();
    hashCode = (37 * hashCode) + new Float(y).hashCode();
    hashCode = (37 * hashCode) + new Float(z).hashCode();
    return hashCode;
  }

  @Override
  public String toString() {
    return "[" + x + "/" + y + "/" + z + "]";
  }

  public static Vec3D parseVec(String s) throws NumberFormatException {
    s = s.replaceAll("\\[|\\]|\\(|\\)|\\{|\\}", "");
    String[] sa = s.split("/|,|x");
    if (sa.length >= 2) {
      return new Vec3D(Double.parseDouble(sa[0].trim()),
          Double.parseDouble(sa[1].trim()), sa.length > 2 ? Double.parseDouble(sa[2]
              .trim()) : 0);
    } else {
      throw new NumberFormatException("Unable to parse '" + s + "' to Vec3D");
    }
  }

}
