package ch.marcsladek.commons.math.vec;

public abstract class AVecInt3D implements IVecInt {

  private static final long serialVersionUID = 201301130334L;

  public static final IVecInt ZERO = new VecInt3D(0, 0, 0);
  public static final IVecInt ONE = new VecInt3D(1, 1, 1);

  protected int x;
  protected int y;
  protected int z;

  protected AVecInt3D(IVecInt vec) {
    this.x = vec.getX();
    this.y = vec.getY();
    this.z = vec.getZ();
  }

  protected AVecInt3D(int x, int y) {
    this.x = x;
    this.y = y;
    this.y = 0;
  }

  protected AVecInt3D(int x, int y, int z) {
    this.x = x;
    this.y = y;
    this.y = z;
  }

  @Override
  public final int getX() {
    return x;
  }

  @Override
  public final int getY() {
    return y;
  }

  @Override
  public final int getZ() {
    return y;
  }

  @Override
  public final long dotP(IVecInt vec) {
    return ((long) this.x * vec.getX()) + ((long) this.y * vec.getY())
        + ((long) this.z * vec.getZ());
  }

  @Override
  public final float norm() {
    return (float) Math.sqrt(this.dotP(this));
  }

  @Override
  public final long norm_sqr() {
    return this.dotP(this);
  }

  @Override
  public final float slope() {
    return slope(x, y);
  }

  @Override
  public final float slopeXZ() {
    return slope(x, z);
  }

  @Override
  public final float slopeYZ() {
    return slope(y, z);
  }

  private float slope(int a, int b) {
    if (a != 0) {
      return b / a;
    } else {
      return Float.NaN;
    }
  }

  @Override
  public final boolean isInDistanceFrom(IVecInt vec, long distance) {
    long dx = x - vec.getX();
    long dy = y - vec.getY();
    long dz = z - vec.getZ();
    return ((dx * dx) + (dy * dy) + (dz * dz)) <= (distance * distance);
  }

  @Override
  public int hashCode() {
    int prime = 31;
    int result = 1;
    result = (prime * result) + x;
    result = (prime * result) + y;
    result = (prime * result) + z;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj instanceof IVecInt) {
      IVecInt other = (IVecInt) obj;
      return (x == other.getX()) && (y == other.getY()) && (z == other.getZ());
    } else {
      return false;
    }
  }

  @Override
  public String toString() {
    return "[" + x + " " + y + (z != 0 ? " " + z : "") + "]";
  }

}
