package ch.marcsladek.commons.math.vec;

import java.io.Serializable;

public final class Vec implements Serializable {

  private static final long serialVersionUID = 201306061705L;

  public static final Vec ZERO = new Vec(0, 0);
  public static final Vec ONE = new Vec(1, 1);

  private double x;
  private double y;

  public Vec(Vec vec) {
    this.x = vec.x;
    this.y = vec.y;
  }

  public Vec(double x, double y) {
    this.x = x;
    this.y = y;
  }

  public double getX() {
    return x;
  }

  public double getY() {
    return y;
  }

  public int getXInt() {
    return (int) Math.round(x);
  }

  public int getYInt() {
    return (int) Math.round(y);
  }

  public Vec copy() {
    return new Vec(this);
  }

  public Vec add(Vec vec) {
    x += vec.x;
    y += vec.y;
    return this;
  }

  public Vec sub(Vec vec) {
    x -= vec.x;
    y -= vec.y;
    return this;
  }

  public Vec mult(double s) {
    x *= s;
    y *= s;
    return this;
  }

  public Vec div(double s) {
    checkZeroDivision(s);
    x /= s;
    y /= s;
    return this;
  }

  public double dotP(Vec vec) {
    return (x * vec.x) + (y * vec.y);
  }

  public double norm() {
    return Math.sqrt(this.dotP(this));
  }

  public double normSqr() {
    return this.dotP(this);
  }

  public double slope() {
    checkZeroDivision(x);
    return y / x;
  }

  public Vec invert() {
    double tmp = x;
    x = y;
    y = tmp;
    return this;
  }

  public Vec negateX() {
    x *= -1;
    return this;
  }

  public Vec negateY() {
    y *= -1;
    return this;
  }

  public Vec negate() {
    return this.negateX().negateY();
  }

  public Vec absX() {
    x = Math.abs(x);
    return this;
  }

  public Vec absY() {
    y = Math.abs(y);
    return this;
  }

  public Vec abs() {
    return this.absX().absY();
  }

  public Vec normalise() {
    return this.div(this.norm());
  }

  public double angle(Vec vec) {
    return Math.acos(this.dotP(vec) / (this.norm() * vec.norm()));
  }

  public double distance(Vec vec) {
    return Math.sqrt(this.distanceSqr(vec));
  }

  public double distanceSqr(Vec vec) {
    double dx = x - vec.x;
    double dy = y - vec.y;
    return (dx * dx) + (dy * dy);
  }

  public boolean isInDistanceTo(Vec vec, double distance) {
    return this.distanceSqr(vec) <= (distance * distance);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    } else if (obj instanceof Vec) {
      return equals((Vec) obj, 0);
    } else {
      return false;
    }
  }

  public boolean equalsInt(Vec vec) {
    return (Math.round(x) == Math.round(vec.x)) && (Math.round(y) == Math.round(vec.y));
  }

  public boolean equals(Vec vec, int decimalPlace) {
    return (round(x, decimalPlace) == round(vec.x, decimalPlace))
        && (round(y, decimalPlace) == round(vec.y, decimalPlace));
  }

  private double round(double val, int decimalPlace) {
    if (decimalPlace > 0) {
      int val_int = (int) val;
      double nb = Math.pow(10, decimalPlace);
      return val_int + (Math.round((val - val_int) * nb) / nb);
    } else {
      return val;
    }
  }

  @Override
  public int hashCode() {
    int hashCode = 17;
    hashCode = (37 * hashCode) + new Float(x).hashCode();
    hashCode = (37 * hashCode) + new Float(y).hashCode();
    return hashCode;
  }

  @Override
  public String toString() {
    return "[" + x + "/" + y + "]";
  }

  /**
   * solves for vectors v1, d1, v2, d2 and values r, s the equation:<br>
   * v1 + r*d1 = v2 + s*d2
   * 
   * @param v1
   *          vector for point 1
   * @param d1
   *          direction vector from point 1
   * @param v2
   *          vector for point 2
   * @param d2
   *          direction vector from point 2
   * @return value r from upper equation
   */
  public static double intersection(Vec v1, Vec d1, Vec v2, Vec d2) {
    double divisor = (d1.y * d2.x) - (d1.x * d2.y);
    checkZeroDivision(divisor);
    return (((v1.x * d2.y) - (v1.y * d2.x) - (v2.x * d2.y)) + (v2.y * d2.x)) / divisor;
  }

  public static Vec parseVec(String s) throws NumberFormatException {
    s = s.replaceAll("\\[|\\]|\\(|\\)|\\{|\\}", "");
    String[] sa = s.split("/|,|x");
    if (sa.length == 2) {
      return new Vec(Double.parseDouble(sa[0].trim()), Double.parseDouble(sa[1].trim()));
    } else {
      throw new NumberFormatException("Unable to parse '" + s + "' to Vec");
    }
  }

  private static void checkZeroDivision(double val) throws ArithmeticException {
    if (val == 0) {
      throw new ArithmeticException("Division by zero");
    }
  }

}
