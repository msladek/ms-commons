package ch.marcsladek.commons.util.config;

public class IllegalPropertyTypeException extends Exception {

  private static final long serialVersionUID = 201309281746L;

  IllegalPropertyTypeException(String message) {
    super(message);
  }

}
