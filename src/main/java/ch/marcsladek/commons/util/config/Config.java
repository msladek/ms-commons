package ch.marcsladek.commons.util.config;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public class Config {

  private final PropertiesConfiguration config;
  private final List<Property> propertyList;

  public Config(Path configPath, Class<? extends Config> clazz) throws ConfigurationException,
      IllegalAccessException {
    this(configPath, getPropertyList(clazz));
  }

  private static List<Property> getPropertyList(Class<? extends Config> clazz)
      throws IllegalAccessException {
    List<Property> propertyList = new ArrayList<>();
    for (Field field : clazz.getDeclaredFields()) {
      if (field.getType() == Property.class) {
        Property property = (Property) field.get(null);
        propertyList.add(property);
      }
    }
    return propertyList;
  }

  public Config(Path configPath, List<Property> propertyList) throws ConfigurationException {
    this.config = new PropertiesConfiguration(configPath.toFile());
    this.propertyList = new ArrayList<>(propertyList);
    this.save();
  }

  private void save() throws ConfigurationException {
    for (Property property : propertyList) {
      if (!config.containsKey(property.getName())) {
        config.addProperty(property.getName(), parseValue(property.getDefaultValue()));
      }
    }
    config.save();
  }

  public void createDirs() throws IOException {
    for (Property property : propertyList) {
      if (property.getDefaultValue() instanceof Path) {
        String value = config.getString(property.getName());
        try {
          Files.createDirectories(Paths.get(value));
        } catch (InvalidPathException exc) {
          throw new IOException("Unable to create Path from '" + value + "'", exc);
        }
      }
    }
  }

  public void addProperty(Property property, Object value) throws ConfigurationException,
      IllegalPropertyTypeException {
    if (property.getDefaultValue().getClass() == value.getClass()) {
      config.addProperty(property.getName(), parseValue(value));
    } else {
      throw createException(property, value.getClass());
    }
    config.save();
  }

  private Object parseValue(Object value) {
    if (value instanceof Path) {
      value = value.toString();
    }
    return value;
  }

  public Object getObject(Property property) throws IllegalPropertyTypeException {
    return config.getProperty(property.getName());
  }

  public boolean getBoolean(Property property) throws IllegalPropertyTypeException {
    if (property.getDefaultValue() instanceof Boolean) {
      return config.getBoolean(property.getName());
    } else {
      throw createException(property, Boolean.class);
    }
  }

  public byte getByte(Property property) throws IllegalPropertyTypeException {
    if (property.getDefaultValue() instanceof Byte) {
      return config.getByte(property.getName());
    } else {
      throw createException(property, Byte.class);
    }
  }

  public short getShort(Property property) throws IllegalPropertyTypeException {
    if (property.getDefaultValue() instanceof Short) {
      return config.getShort(property.getName());
    } else {
      throw createException(property, Short.class);
    }
  }

  public int getInt(Property property) throws IllegalPropertyTypeException {
    if (property.getDefaultValue() instanceof Integer) {
      return config.getInt(property.getName());
    } else {
      throw createException(property, Integer.class);
    }
  }

  public long getLong(Property property) throws IllegalPropertyTypeException {
    if (property.getDefaultValue() instanceof Long) {
      return config.getLong(property.getName());
    } else {
      throw createException(property, Long.class);
    }
  }

  public float getFloat(Property property) throws IllegalPropertyTypeException {
    if (property.getDefaultValue() instanceof Float) {
      return config.getFloat(property.getName());
    } else {
      throw createException(property, Float.class);
    }
  }

  public double getDouble(Property property) throws IllegalPropertyTypeException {
    if (property.getDefaultValue() instanceof Double) {
      return config.getDouble(property.getName());
    } else {
      throw createException(property, Double.class);
    }
  }

  public String getString(Property property) throws IllegalPropertyTypeException {
    if (property.getDefaultValue() instanceof String) {
      return config.getString(property.getName());
    } else {
      throw createException(property, String.class);
    }
  }

  public Path getPath(Property property) throws IllegalPropertyTypeException {
    if (property.getDefaultValue() instanceof Path) {
      return Paths.get(config.getString(property.getName()));
    } else {
      throw createException(property, Path.class);
    }
  }

  private IllegalPropertyTypeException createException(Property property, Class<?> clazz) {
    return new IllegalPropertyTypeException("Property '" + property.getName()
        + "' is not of type '" + clazz.getSimpleName() + "', but '"
        + property.getDefaultValue().getClass().getSimpleName() + "'");
  }

}
