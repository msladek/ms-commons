package ch.marcsladek.commons.util.config;

public class Property {

  private final String name;
  private final Object defaultValue;

  public Property(String name, Object defaultValue) {
    this.name = name;
    this.defaultValue = defaultValue;
  }

  public String getName() {
    return name;
  }

  public Object getDefaultValue() {
    return defaultValue;
  }

  @Override
  public String toString() {
    return "Property [name=" + name + ", defaultValue=" + defaultValue + "]";
  }

}
