package ch.marcsladek.commons.util.map;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListMap<K, V> implements Serializable {

  private static final long serialVersionUID = 201205090109L;

  private Map<K, List<V>> map;
  private int maxListSize;

  public ListMap() {
    this(-1);
  }

  public ListMap(int maxListSize) {
    this.maxListSize = maxListSize;
    this.map = new HashMap<K, List<V>>();
  }

  public int getMaxListSize() {
    return maxListSize;
  }

  public void setMaxListSize(int maxListSize) {
    this.maxListSize = maxListSize;
  }

  public void add(K key, V val) {
    List<V> list = map.get(key);
    if (list == null) {
      list = new ArrayList<V>();
      map.put(key, list);
    } else if ((maxListSize > 0) && (list.size() >= maxListSize)) {
      list.remove(0);
    }
    list.add(val);
  }

  public V get(K key, int i) {
    List<V> list = map.get(key);
    if (i < list.size()) {
      return list.get(list.size() - 1 - i);
    } else {
      return null;
    }
  }

  public V remove(K key, int i) {
    List<V> list = map.get(key);
    if (i < list.size()) {
      return list.remove(list.size() - 1 - i);
    } else {
      return null;
    }
  }

  public boolean setToFirst(K key, int i) {
    List<V> list = map.get(key);
    if (i < list.size()) {
      return list.add(list.remove(list.size() - 1 - i));
    } else {
      return false;
    }
  }

  public int getSize() {
    return map.size();
  }

  public int getSize(K key) {
    List<V> list = map.get(key);
    if (list != null) {
      return list.size();
    } else {
      return 0;
    }
  }

  public void clear() {
    map.clear();
  }

  public void clear(K key) {
    List<V> list = map.get(key);
    if (list != null) {
      list.clear();
    }
  }

  public int indexOf(K key, V val) {
    List<V> list = map.get(key);
    if (list != null) {
      return list.indexOf(val);
    } else {
      return -1;
    }
  }

  @Override
  public String toString() {
    return "ListMap [maxListSize=" + maxListSize + ", map=" + map + "]";
  }
}
