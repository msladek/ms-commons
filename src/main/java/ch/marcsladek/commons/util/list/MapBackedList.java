package ch.marcsladek.commons.util.list;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * This class extends {@link ArrayList} and is backed by a {@link HashMap}, therefore
 * allowing {@link #contains(Object)} and {@link #indexOf(Object)} calls with complexity
 * O(1), at the expense of higher constant time and memory costs than a regular
 * {@link ArrayList}.
 * 
 * @author Morrow
 * @param <E>
 *          List elements type
 */
public class MapBackedList<E> extends ArrayList<E> {

  private static final long serialVersionUID = 201305141341L;

  private Map<E, Integer> map;

  public MapBackedList(int initialCapacity) {
    super(initialCapacity);
    map = new HashMap<>();
  }

  public MapBackedList() {
    super();
    map = new HashMap<>();
  }

  public MapBackedList(Collection<? extends E> c) {
    super();
    addAll(c);
  }

  Map<E, Integer> getMap() {
    return map;
  }

  @Override
  public boolean contains(Object o) {
    return map.containsKey(o);
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    boolean containsAll = true;
    for (Object o : c) {
      containsAll &= map.containsKey(o);
    }
    return containsAll;
  }

  @Override
  public int indexOf(Object o) {
    Integer index = map.get(o);
    return index != null ? index : -1;
  }

  @Override
  public int lastIndexOf(Object o) {
    // elements are unique, there only one index per object
    return indexOf(o);
  }

  @Override
  public boolean add(E e) {
    if (!map.containsKey(e)) {
      super.add(e);
      map.put(e, this.size() - 1);
      return true;
    } else {
      return false;
    }
  }

  @Override
  public void add(int index, E element) {
    // TODO
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean addAll(Collection<? extends E> c) {
    boolean changed = false;
    for (E e : c) {
      changed |= add(e);
    }
    return changed;
  }

  @Override
  public boolean addAll(int index, Collection<? extends E> c) {
    // TODO
    throw new UnsupportedOperationException();
  }

  @Override
  public E set(int index, E element) {
    E prevElement = this.get(index);
    super.set(index, element);
    map.remove(prevElement);
    map.put(element, index);
    return prevElement;
  }

  @Override
  public E remove(int index) {
    E removedElement = this.get(index);
    map.remove(removedElement);
    int lastIndex = this.size() - 1;
    for (; index < lastIndex; index++) {
      E toShift = this.get(index + 1);
      super.set(index, toShift);
      map.put(toShift, index);
    }
    super.remove(lastIndex);
    return removedElement;
  }

  @Override
  public boolean remove(Object o) {
    Integer index = map.get(o);
    if (index != null) {
      remove(index.intValue());
      return true;
    } else {
      return false;
    }
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    boolean changed = false;
    for (Object o : c) {
      changed |= remove(o);
    }
    return changed;
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    int prevSize = this.size();
    Iterator<E> iter = this.iterator();
    while (iter.hasNext()) {
      E e = iter.next();
      if (!c.contains(e)) {
        iter.remove();
      }
    }
    return prevSize != this.size();
  }

  @Override
  public void clear() {
    super.clear();
    map.clear();
  }

  @Override
  public List<E> subList(int fromIndex, int toIndex) {
    // TODO
    throw new UnsupportedOperationException();
  }

  public String toStringMap() {
    return map.toString();
  }

}
