package ch.marcsladek.commons.util.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * @author Marc Sladek
 */
public class SortedList<K extends Comparable<K>, E> implements Iterable<E>, Serializable {

  private static final long serialVersionUID = 201212130228L;

  List<SortedListEntry<K, E>> list;

  public SortedList() {
    list = new ArrayList<SortedListEntry<K, E>>();
  }

  public int size() {
    return list.size();
  }

  public boolean add(K key, E element) {
    SortedListEntry<K, E> entry = new SortedListEntry<K, E>(key, element);
    boolean ret = list.add(entry);
    Collections.sort(list);
    return ret;
  }

  public E remove(K key) {
    for (SortedListEntry<K, E> entry : list) {
      if (entry.getKey().equals(key) && list.remove(entry)) {
        return entry.getValue();
      }
    }
    return null;
  }

  public E remove(int index) {
    return list.remove(index).getValue();
  }

  public E get(int index) {
    return list.get(index).getValue();
  }

  public E get(K key) {
    for (SortedListEntry<K, E> entry : list) {
      if (entry.getKey().equals(key)) {
        return entry.getValue();
      }
    }
    return null;
  }

  @Override
  public Iterator<E> iterator() {
    return new Iterator<E>() {

      private int currentIndex = 0;

      @Override
      public boolean hasNext() {
        return currentIndex < list.size();
      }

      @Override
      public E next() {
        return list.get(currentIndex++).getValue();
      }

      @Override
      public void remove() {
        list.remove(--currentIndex).getValue();
      }
    };
  }

  class SortedListEntry<L extends Comparable<L>, F> implements
      Comparable<SortedListEntry<L, F>>, Serializable {

    private static final long serialVersionUID = 201212130229L;

    private final L key;
    private final F element;

    SortedListEntry(L key, F element) {
      this.key = key;
      this.element = element;
    }

    L getKey() {
      return key;
    }

    F getValue() {
      return element;
    }

    @Override
    public int compareTo(SortedListEntry<L, F> o) {
      return key.compareTo(o.key);
    }
  }

}
