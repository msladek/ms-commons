package ch.marcsladek.commons.util.tree;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 
 * TreeNode represents a Node in the Tree and stores an element, a parent reference and a
 * list of children
 * 
 * @author Marc Sladek
 * 
 * @param <E>
 *          Type of element
 */
public class TreeNode<E> implements Serializable {

  private static final long serialVersionUID = 201212130239L;

  private final String name;
  private final E element;

  private TreeNode<E> parent;
  private final List<TreeNode<E>> children;

  public TreeNode(String name, E element) {
    this.name = Objects.requireNonNull(name, "Name is null");
    this.element = Objects.requireNonNull(element, "Element is null");
    children = new ArrayList<TreeNode<E>>();
  }

  public boolean isRoot() {
    return parent == null;
  }

  /**
   * adds child to the children list and sets the parent of child. <br>
   * 
   * @param child
   *          TreeNode which has to be set as a child. May not be null.
   * @throws IllegalArgumentException
   *           if child has already set a parent.
   */
  public void addChild(TreeNode<E> child) {
    if (child.parent == null) {
      children.add(child);
      child.parent = this;
    } else {
      throw new IllegalArgumentException("Cannot add '" + child + "' as child to '" + this
          + "', it already has parent " + child.parent + "'");
    }
  }

  /**
   * adds the sub child to a children list denoted by indexes and sets the parent of
   * child. <br>
   * 
   * @param indexes
   *          for navigation to sub child.
   * @param subChild
   *          TreeNode which has to be set as a sub child. May not be null.
   * @throws IllegalArgumentException
   *           if child has already set a parent.
   * @throws IndexOutOfBoundsException
   *           if index > size || index < 0
   */
  public void addSubChild(int[] indexes, TreeNode<E> subChild) {
    TreeNode<E> addTo = this;
    if (indexes != null) {
      for (int index : indexes) {
        addTo = addTo.getChild(index);
      }
    }
    addTo.addChild(subChild);
  }

  /**
   * @return the nodes element
   */
  public String getName() {
    return name;
  }

  /**
   * @return the nodes element
   */
  public String getPath() {
    return (parent == null ? "" : parent.getPath() + "/") + name;
  }

  /**
   * @return the nodes element
   */
  public E getElement() {
    return element;
  }

  /**
   * @return the nodes parent
   */
  public TreeNode<E> getParent() {
    return parent;
  }

  /**
   * @param index
   * @return child for given index
   * @throws IndexOutOfBoundsException
   *           if index > size || index < 0
   */
  public TreeNode<E> getChild(int index) {
    return children.get(index);
  }

  /**
   * @param name
   * @return child for given name or null if not existent
   */
  public TreeNode<E> getChild(String name) {
    for (TreeNode<E> child : children) {
      if (child.name.equals(name)) {
        return child;
      }
    }
    return null;
  }

  /**
   * @param child
   * @return index which equals given child, -1 if not existent
   */
  public int indexOf(TreeNode<E> child) {
    return children.indexOf(element);
  }

  /**
   * @return List with children
   */
  public List<TreeNode<E>> getChildren() {
    return new ArrayList<TreeNode<E>>(children);
  }

  /**
   * @param index
   * @return removed child for given index
   * @throws IndexOutOfBoundsException
   *           if index > size || index < 0
   */
  public TreeNode<E> removeChild(int index) {
    return children.remove(index);
  }

  public int getChildrenSize() {
    return children.size();
  }

  @Override
  public String toString() {
    return "TreeNode " + name + ", " + element;
  }

  public String toStringChildren() {
    return toString("");
  }

  private String toString(String tab) {
    String ret = tab + "TreeNode " + name + ", " + element + " [\n";
    for (TreeNode<E> child : children) {
      ret += child.toString(tab + "\t") + "\n";
    }
    return ret + tab + "]";
  }

  @Override
  public int hashCode() {
    List<Object> list = new ArrayList<Object>();
    list.add(element);
    list.add(parent == null ? null : parent.element);
    for (TreeNode<E> child : children) {
      list.add(child == null ? null : child.element);
    }
    return Objects.hash(list.toArray(new Object[list.size()]));
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if ((obj == null) || (getClass() != obj.getClass())) {
      return false;
    }
    @SuppressWarnings("unchecked")
    TreeNode<E> other = (TreeNode<E>) obj;
    if ((parent == null) || (other.parent == null)) {
      if (parent != other.parent) {
        return false;
      }
    } else if (parent.getElement() != other.parent.getElement()) {
      return false;
    }
    if (!Objects.equals(element, other.element)) {
      return false;
    }
    if (children.size() != other.children.size()) {
      return false;
    }
    for (int i = 0; i < children.size(); i++) {
      if (children.get(i).getElement() != other.children.get(i).getElement()) {
        return false;
      }
    }
    return true;
  }

}
