package ch.marcsladek.commons.util.tree.sorted;

import java.io.Serializable;

import ch.marcsladek.commons.util.list.SortedList;

/**
 * 
 * @author Marc Sladek
 * 
 *         TreeNode represents a Node in the Tree and stores a value, a parent reference
 *         and a sorted list of children
 * 
 * @param <K>
 *          Type of comparable key after which the children list will be sorted
 * @param <E>
 *          Implementation of TreeElement
 */
public class SortedTreeNode<K extends Comparable<K>, E extends SortedTreeElement<K>> implements
    Serializable {

  private static final long serialVersionUID = 201212130239L;

  private final E value;

  private SortedTreeNode<K, E> parent;
  private final SortedList<K, SortedTreeNode<K, E>> children;

  public SortedTreeNode(E value) {
    this.value = value;
    children = new SortedList<K, SortedTreeNode<K, E>>();
  }

  public boolean isRoot() {
    return parent == null;
  }

  /**
   * adds child to the children list and sets the parent of child. <br>
   * Complexity: O(n^2 log n) worst case, but most of the times will be n log n
   * 
   * @param child
   *          TreeNode which has to be set as a child. May not be null.
   * @throws IllegalArgumentException
   *           if child has already set a parent.
   */
  public void addChild(SortedTreeNode<K, E> child) {
    if (child.isRoot()) {
      children.add(child.value.getSortKey(), child);
      child.parent = this;
    } else {
      throw new IllegalArgumentException("Cannot add '" + child.toStringSimple()
          + "' as child to '" + this.toStringSimple() + "', it already has parent "
          + child.parent.toStringSimple() + "'");
    }
  }

  /**
   * adds the sub child to a children list denoted by keys and sets the parent of child. <br>
   * Complexity: Complexity: O(n^4 log n) worst case, but most of the times will be n^3
   * log n
   * 
   * @param keys
   *          for navigation to sub child.
   * @param subChild
   *          TreeNode which has to be set as a sub child. May not be null.
   * @throws IllegalArgumentException
   *           if child has already set a parent or a given key does not exists.
   */
  public void addSubChild(K[] keys, SortedTreeNode<K, E> subChild) {
    SortedTreeNode<K, E> addTo = this;
    if (keys != null) {
      for (K key : keys) {
        addTo = addTo.getChild(key);
        if (addTo == null) {
          throw new IllegalArgumentException("No child for key '" + key + "'");
        }
      }
    }
    addTo.addChild(subChild);
  }

  /**
   * adds the sub child to a children list denoted by indexes and sets the parent of
   * child. <br>
   * Complexity: O(n^3 log n) worst case, but most of the times will be n^2 log n
   * 
   * @param indexes
   *          for navigation to sub child.
   * @param subChild
   *          TreeNode which has to be set as a sub child. May not be null.
   * @throws IllegalArgumentException
   *           if child has already set a parent.
   * @throws IndexOutOfBoundsException
   *           if index > size
   */
  public void addSubChild(int[] indexes, SortedTreeNode<K, E> subChild) {
    SortedTreeNode<K, E> addTo = this;
    if (indexes != null) {
      for (int index : indexes) {
        addTo = addTo.getChild(index);
      }
    }
    addTo.addChild(subChild);
  }

  /**
   * @return the nodes value
   */
  public E getValue() {
    return value;
  }

  /**
   * @return the nodes parent
   */
  public SortedTreeNode<K, E> getParent() {
    return parent;
  }

  /**
   * Complexity: O(n)
   * 
   * @param key
   *          may not be null.
   * @return child for given key
   */
  public SortedTreeNode<K, E> getChild(K key) {
    return children.get(key);
  }

  /**
   * Complexity: O(1)
   * 
   * @param index
   * @return child for given index
   * @throws IndexOutOfBoundsException
   *           if index > size
   */
  public SortedTreeNode<K, E> getChild(int index) {
    return children.get(index);
  }

  /**
   * Complexity: O(n)
   * 
   * @param key
   *          may not be null.
   * @return removed child for given key or null if inexistent
   */
  public SortedTreeNode<K, E> removeChild(K key) {
    return children.remove(key);
  }

  /**
   * Complexity: O(1)
   * 
   * @param index
   * @return removed child for given index
   * @throws IndexOutOfBoundsException
   *           if index > size
   */
  public SortedTreeNode<K, E> removeChild(int index) {
    return children.remove(index);
  }

  public int getChildrenSize() {
    return children.size();
  }

  @Override
  public String toString() {
    return toString("");
  }

  private String toString(String tab) {
    String ret = tab + "TreeNode " + value + " [\n";
    for (SortedTreeNode<K, E> child : children) {
      ret += child.toString(tab + "\t") + "\n";
    }
    return ret + tab + "]";
  }

  public String toStringSimple() {
    return "TreeNode " + value;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = (prime * result) + ((value == null) ? 0 : value.hashCode());
    result = (prime * result)
        + (((parent == null) || (parent.value == null)) ? 0 : parent.value.hashCode());
    for (SortedTreeNode<K, E> child : children) {
      result = (prime * result) + ((child == null) ? 0 : child.hashCode());
    }
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if ((obj == null) || (getClass() != obj.getClass())) {
      return false;
    }
    @SuppressWarnings("unchecked")
    SortedTreeNode<K, E> other = (SortedTreeNode<K, E>) obj;
    if (!(parent == other.parent)) {
      return false;
    }
    if (value == null) {
      if (other.value != null) {
        return false;
      }
    } else if (!value.equals(other.value)) {
      return false;
    }
    if (children.size() != other.children.size()) {
      return false;
    }
    for (int i = 0; i < children.size(); i++) {
      if (children.get(i).getValue().equals(other.children.get(i).getValue())) {
        return false;
      }
    }
    return true;
  }

}
