package ch.marcsladek.commons.util.tree;

/**
 * Represents the results returnable in {@link TreeVisitor} by
 * {@link TreeVisitor#preVisitNode preVisitNode} and {@link TreeVisitor#postVisitNode
 * postVisitNode}
 * 
 * @author Marc Sladek
 * 
 */
public enum TreeVisitResult {

  /**
   * Continues as intended.
   */
  CONTINUE,

  /**
   * Terminate instantaneously.
   */
  TERMINATE,

  /**
   * Continue without visiting the children of this node. This result is only meaningful
   * when returned from the {@link TreeVisitor#preVisitNode} method; otherwise this result
   * type is the same as returning {@link #CONTINUE}.
   */
  SKIP_CHILDREN;
}
