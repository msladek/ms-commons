package ch.marcsladek.commons.util.tree;

/**
 * 
 * This interface is implemented to define the behaviour when using {@link TreeWalker} for
 * traversing {@link TreeNode}s.
 * 
 * @author Marc Sladek
 * 
 * @param <E>
 *          The type of the {@link TreeNode}'s elements
 */
public interface TreeVisitor<E> {

  /**
   * Is called before visiting the node's children recursively.
   * 
   * @param element
   *          of the {@link TreeNode}
   * @return a {@link TreeVisitResult}. May not be null.
   */
  public TreeVisitResult preVisitNode(E element);

  /**
   * Is called after visiting the node's children recursively.
   * 
   * @param element
   *          of the {@link TreeNode}
   * @return a {@link TreeVisitResult}. May not be null.
   */
  public TreeVisitResult postVisitNode(E element);

}
