package ch.marcsladek.commons.util.tree;

import java.util.List;
import java.util.Objects;

/**
 * Traverses a {@link TreeNode} and it's children recursively. The behaviour while
 * traversing is defined by implementing {@link TreeVisitor}.
 * 
 * @author Marc Sladek
 * 
 * @param <E>
 *          The type of the {@link TreeNode}'s elements
 */
public class TreeWalker<E> {

  /**
   * Walks the given {@code tree} with the behaviour defined in {@code visitor}.
   * 
   * @param tree
   *          The {@link TreeNode} to be walked. May not be null.
   * @param visitor
   *          Defines the behaviour while walking. May not be null.
   * @return {@code false} if walking was terminated by the {@code visitor} returning
   *         {@link TreeVisitResult#TERMINATE TERMINATE} at some point, else {@code true}
   * @throws NullPointerException
   *           if {@code visitor} returns {@code null} at some point.
   */
  public boolean walk(TreeNode<E> tree, TreeVisitor<E> visitor) {
    Objects.requireNonNull(tree, "Given TreeNode is null");
    Objects.requireNonNull(visitor, "Given TreeVisitor is null");

    TreeVisitResult result = walkRecursive(tree, visitor);
    Objects.requireNonNull(result, "TreeVisitor returned null");
    return result != TreeVisitResult.TERMINATE ? true : false;
  }

  private TreeVisitResult walkRecursive(TreeNode<E> node, TreeVisitor<E> visitor) {
    TreeVisitResult result = visitor.preVisitNode(node.getElement());
    if (result == TreeVisitResult.CONTINUE) {
      result = visitChildren(node.getChildren(), visitor);
    }
    if ((result != null) && (result != TreeVisitResult.TERMINATE)) {
      result = visitor.postVisitNode(node.getElement());
    }
    return getResult(result);
  }

  private TreeVisitResult visitChildren(List<TreeNode<E>> children, TreeVisitor<E> visitor) {
    for (TreeNode<E> child : children) {
      TreeVisitResult result = walkRecursive(child, visitor);
      if (result == null) {
        return null;
      } else if (result == TreeVisitResult.TERMINATE) {
        return TreeVisitResult.TERMINATE;
      }
    }
    return TreeVisitResult.CONTINUE;
  }

  private TreeVisitResult getResult(TreeVisitResult result) {
    if (result == null) {
      return null;
    } else if (result == TreeVisitResult.TERMINATE) {
      return TreeVisitResult.TERMINATE;
    } else {
      return TreeVisitResult.CONTINUE;
    }
  }

}
