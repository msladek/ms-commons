package ch.marcsladek.commons.util.tree;

public interface TreeBuilder<T> {

  public TreeNode<T> build() throws Exception;

}
