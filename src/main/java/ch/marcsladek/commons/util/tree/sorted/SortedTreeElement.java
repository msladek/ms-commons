package ch.marcsladek.commons.util.tree.sorted;

import java.io.Serializable;

/**
 * 
 * @author Morrow
 * 
 *         TreeElement is an interface for the elements which are supposed to be stored in
 *         the TreeNodes.
 * 
 * @param <K>
 *          Type of comparable key after which the value can be sorted
 */
public interface SortedTreeElement<K extends Comparable<K>> extends Serializable {

  /**
   * @return the key for sorting
   */
  public K getSortKey();

}
