package ch.marcsladek.commons.util;

public class StringUtility {

  public static String removeHead(String str, String head) {
    return str.trim().replaceAll("^" + head, "");
  }

  public static String removeTail(String str, String tail) {
    return str.trim().replaceAll(tail + "$", "");
  }

  public static boolean isDigit(char c, char[] exc) {
    return Character.isDigit(c) || contains(exc, c);
  }

  public static boolean isLetter(char c, char[] exc) {
    return Character.isLetter(c) || contains(exc, c);
  }

  public static boolean isLetterLowerCase(char c, char[] exc) {
    return (Character.isLetter(c) && Character.isLowerCase(c)) || contains(exc, c);
  }

  public static boolean isLetterUpperCase(char c, char[] exc) {
    return (Character.isLetter(c) && Character.isUpperCase(c)) || contains(exc, c);
  }

  public static boolean isLetterOrDigit(char c, char[] exc) {
    return Character.isLetter(c) || Character.isDigit(c) || contains(exc, c);
  }

  public static boolean isLetterUpperCaseOrDigit(char c, char[] exc) {
    return (Character.isLetter(c) && Character.isUpperCase(c)) || Character.isDigit(c)
        || contains(exc, c);
  }

  public static boolean isLetterLowerCaseOrDigit(char c, char[] exc) {
    return (Character.isLetter(c) && Character.isLowerCase(c)) || Character.isDigit(c)
        || contains(exc, c);
  }

  public static boolean isAllDigits(String str, char[] exc) {
    for (char c : str.toCharArray()) {
      if (!isDigit(c, exc)) {
        return false;
      }
    }
    return true;
  }

  public static boolean isAllLetter(String str, char[] exc) {
    for (char c : str.toCharArray()) {
      if (!isLetter(c, exc)) {
        return false;
      }
    }
    return true;
  }

  public static boolean isAllLetterUpperCase(String str, char[] exc) {
    for (char c : str.toCharArray()) {
      if (!isLetterUpperCase(c, exc)) {
        return false;
      }
    }
    return true;
  }

  public static boolean isAllLetterLowerCase(String str, char[] exc) {
    for (char c : str.toCharArray()) {
      if (!isLetterLowerCase(c, exc)) {
        return false;
      }
    }
    return true;
  }

  public static boolean isAllLettersUpperCaseOrDigits(String str, char[] exc) {
    for (char c : str.toCharArray()) {
      if (!isLetterUpperCaseOrDigit(c, exc)) {
        return false;
      }
    }
    return true;
  }

  public static String removeNonLettersUpperCaseOrDigits(String str, char[] exc) {
    StringBuilder ret = new StringBuilder(str);
    for (int i = 0; i < ret.length(); i++) {
      if (!isLetterUpperCaseOrDigit(ret.charAt(i), exc)) {
        ret.deleteCharAt(i--);
      }
    }
    return ret.toString();
  }

  public static String removeISOControlChars(String str) {
    StringBuilder ret = new StringBuilder(str);
    for (int i = 0; i < ret.length(); i++) {
      if (Character.isISOControl(ret.charAt(i))) {
        ret.deleteCharAt(i--);
      }
    }
    return ret.toString();
  }

  private static boolean contains(char[] array, char c) {
    if (array != null) {
      for (char cArray : array) {
        if (cArray == c) {
          return true;
        }
      }
    }
    return false;
  }
}