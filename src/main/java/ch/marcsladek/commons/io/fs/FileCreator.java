package ch.marcsladek.commons.io.fs;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileCreator {

  /**
   * creates files
   * 
   * @param dir
   *          directory of the created files
   * @param amount
   *          amount of files created
   * @param size
   *          size per file in KB
   * @throws IOException
   */
  public static void createFiles(Path dir, int amount, int size) throws IOException {
    String name = "file";
    for (int i = 1; i <= amount; i++) {
      createFile(dir, name + "-" + i, size);
    }
  }

  /**
   * creates a file
   * 
   * @param dir
   *          directory of the created file
   * @param name
   *          name of the file created
   * @param size
   *          size of the file in KB
   * @throws IOException
   */
  public static void createFile(Path dir, String name, int size) throws IOException {
    createFile(dir, name, new byte[1024 * size]);
  }

  /**
   * creates a file
   * 
   * @param dir
   *          directory of the created file
   * @param name
   *          name of the file created
   * @param content
   *          content of the file
   * 
   * @throws IOException
   */
  public static void createFile(Path dir, String name, byte[] content) throws IOException {
    createFile(dir.resolve(name), content);
  }

  /**
   * creates a file
   * 
   * @param path
   *          path of the created file
   * @param content
   *          content of the file
   * 
   * @throws IOException
   */
  public static void createFile(Path path, byte[] content) throws IOException {
    Files.createDirectories(path.getParent());
    OutputStream out = null;
    try {
      out = new BufferedOutputStream(Files.newOutputStream(path));
      out.write(content);
      System.out.println("Created: " + path.getFileName() + " Size: " + (content.length / 1024)
          + "KB");
    } finally {
      try {
        if (out != null) {
          out.close();
        }
      } catch (IOException exc) {
        // silent catch
      }
    }
  }

}
