package ch.marcsladek.commons.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import ch.marcsladek.commons.concurrent.Startable;

public abstract class ConsoleReader extends Thread implements Startable {

  private volatile boolean running;
  private final BufferedReader keyboard;

  protected ConsoleReader() {
    running = false;
    keyboard = new BufferedReader(new InputStreamReader(System.in));
  }

  @Override
  public void run() {
    running = true;
    try {
      String line;
      while (running && ((line = keyboard.readLine()) != null)) {
        process(line.trim());
      }
    } catch (IOException exc) {
      if (running) {
        System.out.println("KeyboardListener: " + exc.getMessage());
      }
    } finally {
      running = false;
    }
  }

  @Override
  public boolean isRunning() {
    return running;
  }

  @Override
  public boolean isTerminated() {
    return !running;
  }

  @Override
  public void shutdown() {
    this.running = false;
    try {
      keyboard.close();
    } catch (IOException e) {
      // silent catch
    }
  }

  protected abstract void process(String line);

}