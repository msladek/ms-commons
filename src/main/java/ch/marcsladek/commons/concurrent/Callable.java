package ch.marcsladek.commons.concurrent;

/**
 * An interface which provides a {@link #call(Object)} method for implementing classes.
 * 
 * @author Marc Sladek
 * 
 * @param <R>
 *          Type of the return object
 * @param <A>
 *          Type of the argument object
 */
public interface Callable<R, A> {

  public R call(A object);

}
