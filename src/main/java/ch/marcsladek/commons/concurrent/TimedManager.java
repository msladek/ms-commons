package ch.marcsladek.commons.concurrent;

import java.util.Iterator;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.tuple.Pair;

/**
 * 
 * <p>
 * Manages {@link TimeManageable}s according to their set start time. They can be enrolled and
 * disenrolled manually. After the allowed time ({@link #allowedTime}) has past for one, it will be
 * automatically disenrolled.
 * </p>
 * 
 * <p>
 * {@link TimeManageable}s will be notified upon disenrollment.
 * </p>
 * 
 * @param <K>
 *          type of identifier of {@link TimeManageable}s
 * @param <V>
 *          class implementing {@link TimeManageable}
 * 
 * @author Marc Sladek
 * 
 */
public class TimedManager<K, V extends TimeManageable<K>> extends Thread implements Startable {

  protected final ConcurrentLinkedQueue<K> queue;
  protected final ConcurrentHashMap<K, Pair<V, Long>> enrolleeMap;

  private volatile boolean started;
  private volatile boolean running;
  private final long allowedTime;

  private final Object lock;

  /**
   * Constructor
   * 
   * @param allowedTime
   *          time after which the manager will force disenroll a {@link TimeManageable}
   * @param unit
   *          unit of the given time
   */
  public TimedManager(long allowedTime, TimeUnit unit) {
    started = running = false;
    queue = new ConcurrentLinkedQueue<>();
    enrolleeMap = new ConcurrentHashMap<>();
    this.allowedTime = unit.toMillis(allowedTime);
    lock = new Object();
  }

  public long getAllowedTime() {
    return allowedTime;
  }

  /**
   * @return amount of enrolled {@link TimeManageable}s
   */
  public int getEnrolleeSize() {
    return enrolleeMap.size();
  }

  /**
   * enrolls the provided enrollee
   * 
   * @param enrollee
   * @return {@code false} if already enrolled
   */
  public boolean enroll(V enrollee) {
    Objects.requireNonNull(enrollee);
    if (running) {
      synchronized (lock) {
        if (enrolleeMap.putIfAbsent(enrollee.getIdentifier(),
            Pair.of(enrollee, System.currentTimeMillis())) == null) {
          queue.add(enrollee.getIdentifier());
          lock.notify();
          return true;
        } else {
          return false;
        }
      }
    } else {
      throw new IllegalStateException("TimedManager is not running");
    }
  }

  /**
   * disenrolls the provided disenrollee
   * 
   * @param disenrollee
   * @return {@code false} if disenrollee is not enrolled
   */
  public boolean disenroll(V disenrollee) {
    Objects.requireNonNull(disenrollee);
    if (running) {
      return disenroll_internal(disenrollee.getIdentifier());
    } else {
      throw new IllegalStateException("TimedManager is not running");
    }
  }

  private boolean disenroll_internal(K key) {
    if (key != null) {
      Pair<V, Long> pair = enrolleeMap.remove(key);
      if (pair != null) {
        pair.getKey().notifyDisenroll(this);
        return true;
      }
    }
    return false;
  }

  @Override
  public void run() {
    started = running = true;
    try {
      while (running && allowedTime > 0) {
        waitOrSleep();
        checkNextEnrolees();
      }
    } finally {
      running = false;
      Iterator<K> iter = queue.iterator();
      while (iter.hasNext()) {
        K key = iter.next();
        iter.remove();
        disenroll_internal(key);
      }
    }
  }

  private void waitOrSleep() {
    try {
      long sleep = 0;
      synchronized (lock) {
        if (queue.isEmpty()) {
          lock.wait();
        } else {
          sleep = getDisenrollTimeForNextEnrolee() - System.currentTimeMillis();
        }
      }
      if (running && sleep > 0) {
        Thread.sleep(sleep);
      }
    } catch (InterruptedException exc) {
      Thread.currentThread().interrupt();
    }
  }

  private void checkNextEnrolees() {
    while (running) {
      if (System.currentTimeMillis() > getDisenrollTimeForNextEnrolee()) {
        disenroll_internal(queue.poll());
      } else {
        break;
      }
    }
  }

  private long getDisenrollTimeForNextEnrolee() {
    K key = queue.peek();
    if (key != null) {
      Pair<V, Long> pair = enrolleeMap.get(key);
      if (pair != null) {
        return pair.getValue() + allowedTime;
      }
    }
    return 0;
  }

  @Override
  public boolean isRunning() {
    return running;
  }

  @Override
  public void shutdown() {
    running = false;
    synchronized (lock) {
      lock.notify();
    }
    this.interrupt();
  }

  @Override
  public boolean isTerminated() {
    return started && !running;
  }

}
