package ch.marcsladek.commons.concurrent;

/**
 * An interface which provides a {@link #start()}, {@link #isRunning()},
 * {@link #shutdown()} and {@link #isTerminated()} method for implementing classes, e.g.
 * Threads or Listeners.
 * 
 * <p>
 * May be used for adding instances to a {@link Startup} object.
 * </p>
 * 
 * @author Marc Sladek
 * 
 */
public interface Startable {

  /**
   * Starts the underlying service.
   */
  public void start() throws Exception;

  /**
   * @return {@code true} if the service is running. This generally means that
   *         {@link #start()} has been called, but not {@link #shutdown()} yet.
   */
  public boolean isRunning();

  /**
   * Shuts down the service.
   */
  public void shutdown();

  /**
   * 
   * @return {@code true} if the service is terminated. This generally means that
   *         {@link #start()} and {@link #shutdown()} have been called.
   */
  public boolean isTerminated();

}
