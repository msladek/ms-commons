package ch.marcsladek.commons.concurrent;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 
 * Binds {@link Callable}s together for a combined {@link #call(Object)} invocation.
 * 
 * <p>
 * This class is useful if several services have to be called at the same time. They are
 * passed to the Constructor {@link Callup#Callup(Callable...)} and are treated as a
 * single, {@link Callable} object.
 * </p>
 * 
 * <p>
 * The class is thread safe. The order of {@link Callable#call(Object)} calls on elements
 * will be in the same order as the argument array.
 * </p>
 * 
 * @author Marc Sladek
 * 
 * @param <A>
 *          Type of the argument object in {@link #call(Object)}
 * 
 */
public class Callup<A> implements Callable<Void, A> {

  private final Object LOCK;
  private final List<Callable<Void, A>> callList;
  private int callCount;

  /**
   * Constructs a {@link Callup} instance with {@code callables} in its list.
   * 
   * @param callables
   *          May not be null.
   */
  @SuppressWarnings("unchecked")
  public Callup(Callable<Void, A>... callables) {
    LOCK = new Object();
    callList = new ArrayList<Callable<Void, A>>();
    for (Callable<Void, A> callable : callables) {
      Objects.requireNonNull(callable);
      callList.add(callable);
    }
    callCount = 0;
  }

  /**
   * @return amount of {@link Callable}'s in the list.
   */
  public int size() {
    synchronized (LOCK) {
      return callList.size();
    }
  }

  /**
   * Calls all {@link Callable}s in the list.
   * 
   * @param object
   *          Is handed over to {@link Callable#call(Object)}.
   */
  @Override
  public Void call(A object) {
    synchronized (LOCK) {
      for (Callable<Void, A> callable : callList) {
        callable.call(object);
      }
      callCount++;
    }
    return null;
  }

  public int getCallCount() {
    synchronized (LOCK) {
      return callCount;
    }
  }

  @Override
  public int hashCode() {
    synchronized (LOCK) {
      return Objects.hash(callList);
    }
  }

  @SuppressWarnings("rawtypes")
  @Override
  public boolean equals(Object obj) {
    synchronized (LOCK) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (obj instanceof Callup) {
        Callup other = (Callup) obj;
        return Objects.equals(callList, other.callList);
      } else {
        return false;
      }
    }
  }

  @Override
  public String toString() {
    return "Callups [callList=" + callList + ", callCount=" + getCallCount() + "]";
  }

}
