package ch.marcsladek.commons.concurrent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class ConcurrentTraverser<E> {

  private final int nThreads;
  private final boolean fair;

  private final ExecutorService executor;
  private final List<Future<?>> futures;

  public ConcurrentTraverser() {
    this(Runtime.getRuntime().availableProcessors(), false);
  }

  public ConcurrentTraverser(int nThreads, boolean fair) {
    this.nThreads = nThreads;
    this.fair = fair;
    executor = Executors.newFixedThreadPool(nThreads);
    futures = new ArrayList<>(nThreads);
  }

  public final ConcurrentTraverser<E> traverse(Collection<? extends E> coll) {
    return traverse(coll.iterator());
  }

  public final ConcurrentTraverser<E> traverse(Iterator<? extends E> iter) {
    if (isCompleted()) {
      Runnable task = new WorkerRunnable(iter, fair);
      for (int i = 0; i < nThreads; i++) {
        futures.add(executor.submit(task));
      }
      return this;
    } else {
      throw new IllegalStateException("Task already running");
    }
  }

  public final boolean isCompleted() {
    Iterator<Future<?>> iter = futures.iterator();
    while (iter.hasNext()) {
      if (iter.next().isDone()) {
        iter.remove();
      } else {
        return false;
      }
    }
    return true;
  }

  public final ConcurrentTraverser<E> awaitCompletion() throws InterruptedException,
      ExecutionException {
    Iterator<Future<?>> iter = futures.iterator();
    while (iter.hasNext()) {
      Future<?> future = iter.next();
      if (!future.isDone()) {
        future.get();
      }
      iter.remove();
    }
    return this;
  }

  public final void shutdown() {
    executor.shutdown();
  }

  public final void shutdownNow() {
    executor.shutdownNow();
  }

  private class WorkerRunnable implements Runnable {

    private final Iterator<? extends E> iter;
    private final Lock lock;

    WorkerRunnable(Iterator<? extends E> iter, boolean fair) {
      this.iter = iter;
      lock = new ReentrantLock(fair);
    }

    @Override
    public void run() {
      E next;
      while (iter.hasNext()) {
        try {
          lock.lock();
          next = iter.next();
        } catch (NoSuchElementException exc) {
          return;
        } finally {
          lock.unlock();
        }
        process(next);
      }
    }
  }

  protected abstract void process(E element);

}
