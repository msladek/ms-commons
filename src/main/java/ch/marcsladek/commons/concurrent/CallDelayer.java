package ch.marcsladek.commons.concurrent;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;


/**
 * This class is intended to be used as a replacement for another {@link Callable}
 * -implementing class, which is provided in the Constructor.
 * {@link Callable#call(Object)} of the underlying object will occur at a minimal rate of
 * the defined time interval. Call objects arriving in between this interval will be
 * collected and handed over to the {@link Callable} as a {@link List} in the next call.
 * 
 * @param <A>
 *          Type of the callable argument
 */
public class CallDelayer<A> implements Callable<Void, A>, Startable {

  private final Callable<Void, List<A>> callable;
  private final int interval;
  private final TimeUnit unit;
  private CallCollector<A> callCollector;

  /**
   * Constructor
   * 
   * @param callable
   *          the underlying Callable object. Parameter has to be a {@link List}. May not
   *          be null.
   * @param interval
   *          the minimal time interval between calls
   * @param unit
   *          the time unit of {@code interval}
   */
  public CallDelayer(Callable<Void, List<A>> callable, int interval, TimeUnit unit) {
    this.callable = Objects.requireNonNull(callable);
    this.interval = interval > 0 ? interval : 0;
    this.unit = Objects.requireNonNull(unit);
  }

  @Override
  public Void call(A object) {
    if (callCollector != null) {
      callCollector.call(object);
    } else {
      throw new IllegalStateException("CallDelayer has not been started yet");
    }
    return null;
  }

  @Override
  public void start() {
    if (callCollector == null) {
      callCollector = new CallCollector<>(callable, interval, unit);
      callCollector.start();
    } else {
      throw new IllegalStateException("CallDelayer has already been started once");
    }
  }

  @Override
  public boolean isRunning() {
    return (callCollector != null) && callCollector.isRunning();
  }

  @Override
  public void shutdown() {
    if (callCollector != null) {
      callCollector.shutdown();
    } else {
      throw new IllegalStateException("CallDelayer has not been started yet");
    }
  }

  @Override
  public boolean isTerminated() {
    return (callCollector != null) && !callCollector.isRunning();
  }

}
