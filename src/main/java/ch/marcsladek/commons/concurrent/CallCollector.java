package ch.marcsladek.commons.concurrent;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;


final class CallCollector<A> extends Thread implements Callable<Boolean, A> {

  private final Object LOCK;
  private final Callable<Void, List<A>> callable;
  private final long interval;
  private final List<A> callObjects;
  private volatile boolean running;

  CallCollector(Callable<Void, List<A>> callable, int interval, TimeUnit unit) {
    LOCK = new Object();
    this.callable = Objects.requireNonNull(callable);
    this.interval = unit.toMillis(interval);
    callObjects = new ArrayList<>();
    running = false;
  }

  boolean isRunning() {
    synchronized (LOCK) {
      return running;
    }
  }

  @Override
  public Boolean call(A object) {
    synchronized (LOCK) {
      if (running && !callObjects.contains(object)) {
        callObjects.add(object);
        LOCK.notify();
        return true;
      }
    }
    return false;
  }

  @Override
  public void start() {
    super.start();
    running = true;
  }

  @Override
  public void run() {
    while (running) {
      boolean sleep;
      synchronized (LOCK) {
        if (sleep = callObjects.size() > 0) {
          recall();
        } else {
          waitForCall();
        }
      }
      if (sleep) {
        sleep();
      }
    }
    running = false;
  }

  private void recall() {
    callable.call(new ArrayList<>(callObjects));
    callObjects.clear();
  }

  private void sleep() {
    try {
      Thread.sleep(interval);
    } catch (InterruptedException exc) {
      Thread.currentThread().interrupt();
    }
  }

  private void waitForCall() {
    try {
      LOCK.wait();
    } catch (InterruptedException exc) {
      Thread.currentThread().interrupt();
    }
  }

  public void shutdown() {
    synchronized (LOCK) {
      running = false;
      LOCK.notify();
    }
  }

}