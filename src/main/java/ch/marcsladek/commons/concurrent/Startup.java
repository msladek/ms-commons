package ch.marcsladek.commons.concurrent;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 
 * Binds {@link Startable}s together for a combined {@link #start()} and {@link #shutdown()}
 * invocation.
 * 
 * <p>
 * This class is useful if several services have to be started and shut down at the same time. They
 * are passed to the Constructor {@link Startup#Startup(Startable...)} and are treated as a single,
 * {@link Startable} object.
 * </p>
 * 
 * <p>
 * The class is thread safe. While there can only be one {@link #start()} call per instance, this
 * restriction does not apply to {@link #shutdown()}. The order of {@link Startable#start()} calls
 * on elements will be in the same order as the argument array. The {@link Startable#shutdown()}
 * calls will be inverted.
 * </p>
 * 
 * @author Marc Sladek
 * 
 */
public class Startup implements Startable {

  /**
   * No {@link Startable} checks are performed (standard).
   */
  public static final int CHECK_MODE_NONE = 0;

  /**
   * Checks every {@link Startable} directly after the start and shutdown call.
   */
  public static final int CHECK_MODE_BEFORE_NEXT = 1;

  /**
   * Checks every {@link Startable} after all the start and shutdown calls.
   */
  public static final int CHECK_MODE_AFTER_ALL = 2;

  /**
   * Checks every {@link Startable} directly after the start call.
   */
  public static final int CHECK_MODE_BEFORE_NEXT_START = 3;

  /**
   * Checks every {@link Startable} after all the start calls.
   */
  public static final int CHECK_MODE_AFTER_ALL_START = 4;

  /**
   * Checks every {@link Startable} directly after the shutdown call.
   */
  public static final int CHECK_MODE_BEFORE_NEXT_SHUTDOWN = 5;

  /**
   * Checks every {@link Startable} after all the shutdown calls.
   */
  public static final int CHECK_MODE_AFTER_ALL_SHUTDOWN = 6;

  private final Object LOCK;
  private final List<Startable> startList;
  private final int checkMode;
  private int checkMaxTime = 10 * 1000;
  private final boolean strict;
  private volatile boolean started;
  private volatile boolean shutDown;

  /**
   * Constructs a Startup instance with {@code startables} in its list.
   * 
   * @param startables
   *          May not be null.
   */
  public Startup(Startable... startables) {
    this(CHECK_MODE_NONE, true, startables);
  }

  /**
   * Constructs a Startup instance with {@code startables} in its list.
   * 
   * @param checkMode
   *          For start and shutdown calls.
   * @param strict
   *          If true, {@link #start} and {@link #shutdown} may throw {@link IllegalStateException}s
   * @param startables
   *          May not be null.
   */
  public Startup(int checkMode, boolean strict, Startable... startables) {
    LOCK = new Object();
    startList = new ArrayList<Startable>();
    for (Startable startable : startables) {
      Objects.requireNonNull(startable);
      startList.add(startable);
    }
    this.checkMode = checkMode;
    this.strict = strict;
    started = false;
    shutDown = false;
  }

  /**
   * @param checkMaxTime
   *          maximal time for check duration.
   */
  public void setCheckMaxTime(int checkMaxTime) {
    synchronized (LOCK) {
      this.checkMaxTime = checkMaxTime >= 0 ? checkMaxTime : 0;
    }
  }

  /**
   * @return amount of {@link Startable}'s in the list.
   */
  public int size() {
    synchronized (LOCK) {
      return startList.size();
    }
  }

  /**
   * Starts all {@link Startable}s in the list.
   * 
   * @throws IllegalStateException
   *           if {@code strict} and {@link #start()} has already been called on the object.
   */
  @Override
  public void start() throws Exception {
    synchronized (LOCK) {
      if (strict && started) {
        throw new IllegalStateException("Start has already been called");
      } else {
        started = true;
        for (Startable startable : startList) {
          startable.start();
          if ((checkMode == CHECK_MODE_BEFORE_NEXT) || (checkMode == CHECK_MODE_BEFORE_NEXT_START)) {
            waitForStartable(startable, true);
          }
        }
        if ((checkMode == CHECK_MODE_AFTER_ALL) || (checkMode == CHECK_MODE_AFTER_ALL_START)) {
          for (Startable startable : startList) {
            waitForStartable(startable, true);
          }
        }
      }
    }
  }

  @Override
  public boolean isRunning() {
    synchronized (LOCK) {
      return started && !shutDown;
    }
  }

  /**
   * Shuts down all {@link Startable}s in the list in an inverted order.
   * 
   * @throws IllegalStateException
   *           if {@code strict} and {@link #start()} has not yet been called on the object.
   */
  @Override
  public void shutdown() {
    synchronized (LOCK) {
      if (strict && !started) {
        throw new IllegalStateException("Start hasn't been called yet");
      } else {
        shutDown = true;
        for (int i = startList.size() - 1; i >= 0; i--) {
          startList.get(i).shutdown();
          if ((checkMode == CHECK_MODE_BEFORE_NEXT)
              || (checkMode == CHECK_MODE_BEFORE_NEXT_SHUTDOWN)) {
            waitForStartable(startList.get(i), false);
          }
        }
        if ((checkMode == CHECK_MODE_AFTER_ALL) || (checkMode == CHECK_MODE_AFTER_ALL_SHUTDOWN)) {
          for (Startable startable : startList) {
            waitForStartable(startable, false);
          }
        }
      }
    }
  }

  @Override
  public boolean isTerminated() {
    synchronized (LOCK) {
      return started && shutDown;
    }
  }

  @Override
  public int hashCode() {
    synchronized (LOCK) {
      return Objects.hash(startList, checkMode);
    }
  }

  @Override
  public boolean equals(Object obj) {
    synchronized (LOCK) {
      if (this == obj) {
        return true;
      }
      if (obj == null) {
        return false;
      }
      if (obj instanceof Startup) {
        Startup other = (Startup) obj;
        return Objects.equals(startList, other.startList) && (checkMode == other.checkMode);
      } else {
        return false;
      }
    }
  }

  @Override
  public String toString() {
    return "Startups [startList=" + startList + ", checkMode=" + checkMode + ", running="
        + isRunning() + ", terminated=" + isTerminated() + "]";
  }

  private void waitForStartable(Startable startable, boolean start) {
    int count = 0;
    do {
      if (start ? startable.isRunning() : startable.isTerminated()) {
        return;
      } else {
        try {
          Thread.sleep(checkMaxTime / 10);
        } catch (InterruptedException exc) {
          break;
        }
      }
    } while (count < 10);
    throw new IllegalStateException("Startable '" + startable + "' didn't start");
  }

}
