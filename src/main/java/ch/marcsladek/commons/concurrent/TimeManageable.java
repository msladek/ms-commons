package ch.marcsladek.commons.concurrent;

/**
 * An interface for classes which should be enrollable at the {@link TimedManager}.
 * 
 * @param <T>
 *          type of identifier
 * 
 * @author Marc Sladek
 * 
 */
public interface TimeManageable<T> {

  /**
   * @return the identifier of this {@link TimeManageable}.
   */
  public T getIdentifier();

  /**
   * notifies the disenrollment at given {@link TimedManager}.
   */
  public void notifyDisenroll(TimedManager<T, ? extends TimeManageable<T>> timedManager);

}
