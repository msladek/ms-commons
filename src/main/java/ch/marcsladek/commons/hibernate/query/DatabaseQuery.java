package ch.marcsladek.commons.hibernate.query;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import ch.marcsladek.commons.hibernate.Field;

public class DatabaseQuery {

  private final String queryString;
  private final Set<Field> fields;
  private final int limit;

  DatabaseQuery(String queryString, Collection<Field> fields, int limit) {
    this.queryString = Objects.requireNonNull(queryString);
    if (fields != null) {
      this.fields = Collections.unmodifiableSet(new HashSet<Field>(fields));
    } else {
      this.fields = Collections.unmodifiableSet(new HashSet<Field>());
    }
    this.limit = limit;
  }

  String getQueryString() {
    return queryString;
  }

  Set<Field> getFields() {
    return fields;
  }

  int getLimit() {
    return limit;
  }

  public BindedDatabaseQuery getBindedDatabaseQuery() {
    return getBindedDatabaseQuery(new HashMap<Field, Object>());
  }

  public BindedDatabaseQuery getBindedDatabaseQuery(Map<Field, Object> binds) {
    Objects.requireNonNull(binds);
    if (fields.size() != binds.size()) {
      throw getException(binds);
    } else if (fields.size() > 0) {
      for (Field field : binds.keySet()) {
        if (!fields.contains(field)) {
          throw getException(binds);
        }
      }
    }
    return new BindedDatabaseQuery(this, binds);
  }

  private IllegalArgumentException getException(Map<Field, Object> binds) {
    return new IllegalArgumentException("Illegal bind fields '" + binds.keySet() + "' for fields '"
        + fields + "'");
  }

  @Override
  public String toString() {
    return "DatabaseQuery [queryString=" + queryString + ", fields=" + fields + ", limit=" + limit
        + "]";
  }
}
