package ch.marcsladek.commons.hibernate.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import ch.marcsladek.commons.hibernate.Field;

public class DatabaseQueryBuilder {

  private final DatabaseQueryType type;
  private final Class<?> clazz;

  private List<Field> andFields;
  private List<List<Field>> orFields;
  private List<Field> orderFields;
  private boolean asc;
  private int limit = 0;

  public DatabaseQueryBuilder(DatabaseQueryType type, Class<?> clazz) {
    this.type = type;
    this.clazz = clazz;
  }

  public DatabaseQueryBuilder(DatabaseQueryType type, Class<?> clazz, List<Field> fields) {
    this.type = type;
    this.clazz = clazz;
    addFields(fields);
  }

  public void addField(Field field) {
    if (andFields == null) {
      andFields = new ArrayList<>();
    }
    andFields.add(field);
  }

  public void addFields(List<Field> fields) {
    if (andFields == null) {
      andFields = new ArrayList<>();
    }
    andFields.addAll(fields);
  }

  public void addOrFields(List<Field> fields) {
    if (orFields == null) {
      orFields = new ArrayList<>();
    }
    orFields.add(fields);
  }

  public void setOrder(List<Field> orderFields, boolean asc) {
    this.orderFields = orderFields;
    this.asc = asc;
  }

  public void revokeOrder() {
    setOrder(null, false);
  }

  public void setLimit(int limit) {
    if (limit >= 0) {
      this.limit = limit;
    } else {
      throw new IllegalArgumentException("only positive or 0 allowed as limit");
    }
  }

  public DatabaseQuery build() {
    Collection<Field> fields = new HashSet<>();
    if (andFields != null) {
      fields.addAll(andFields);
    }
    if (orFields != null) {
      for (List<Field> orSubFields : orFields) {
        fields.addAll(orSubFields);
      }
    }
    return new DatabaseQuery(generateQueryString(), fields, limit);
  }

  private String generateQueryString() {
    StringBuilder sb = new StringBuilder();
    if (type == DatabaseQueryType.COUNT) {
      sb.append("SELECT COUNT(id) ");
    }
    sb.append("FROM ").append(clazz.getSimpleName());
    String andString = getAndString();
    String orString = getOrString();
    if (andString.length() > 0 || orString.length() > 0) {
      sb.append(" WHERE ");
      sb.append(andString);
      if (andString.length() > 0 && orString.length() > 0) {
        sb.append(" AND ");
      }
      sb.append(orString);
    }
    String orderString = getOrderString();
    if (orderString.length() > 0) {
      sb.append(" ORDER BY ");
      sb.append(orderString);
    }
    return sb.toString();
  }

  private String getAndString() {
    StringBuilder sb = new StringBuilder();
    if (andFields != null && andFields.size() > 0) {
      for (Field field : andFields) {
        if (sb.length() > 0) {
          sb.append(" AND ");
        }
        sb.append(field + " = :" + field);
      }
    }
    return sb.toString();
  }

  private String getOrString() {
    StringBuilder sb = new StringBuilder();
    if (orFields != null && orFields.size() > 0) {
      for (List<Field> fields : orFields) {
        if (fields != null && fields.size() > 0) {
          if (sb.length() > 0) {
            sb.append(" AND ");
          }
          sb.append(buildOrSubString(fields, orFields.size() == 1));
        }
      }
    }
    return sb.toString();
  }

  private String buildOrSubString(List<Field> orFields, boolean skipBrackets) {
    StringBuilder sb = new StringBuilder();
    if (!skipBrackets) {
      sb.append("(");
    }
    for (Field field : orFields) {
      if (sb.length() > 1) {
        sb.append(" OR ");
      }
      sb.append(field + " = :" + field);
    }
    if (!skipBrackets) {
      sb.append(")");
    }
    return sb.toString();
  }

  private String getOrderString() {
    StringBuilder sb = new StringBuilder();
    if (orderFields != null && orderFields.size() > 0) {
      for (Field field : orderFields) {
        if (sb.length() > 0) {
          sb.append(", ");
        }
        sb.append(field);
      }
      sb.append(asc ? " ASC" : " DESC");
    }
    return sb.toString();
  }

}
