package ch.marcsladek.commons.hibernate.query;

import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import ch.marcsladek.commons.hibernate.Field;

public class BindedDatabaseQuery {

  private final DatabaseQuery query;
  private final Map<Field, Object> binds;

  BindedDatabaseQuery(DatabaseQuery query, Map<Field, Object> binds) {
    this.query = query;
    this.binds = Collections.unmodifiableMap(binds);
  }

  public String getQueryString() {
    return query.getQueryString();
  }

  public int getLimit() {
    return query.getLimit();
  }

  public Set<Entry<Field, Object>> getBinds() {
    return binds.entrySet();
  }

  @Override
  public String toString() {
    return "BindedDatabaseQuery [query=" + query + ", binds=" + binds + "]";
  }

}
