package ch.marcsladek.commons.hibernate.query;

public enum DatabaseQueryType {
  SELECT, COUNT;
}