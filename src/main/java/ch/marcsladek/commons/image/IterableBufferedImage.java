package ch.marcsladek.commons.image;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import ch.marcsladek.commons.math.vec.VecInt2D;

public class IterableBufferedImage extends BufferedImage implements Iterable<VecInt2D> {

  private List<VecInt2D> vecs;

  public IterableBufferedImage(int width, int height, int imageType) {
    this(width, height, imageType, false);
  }

  public IterableBufferedImage(int width, int height, int imageType, boolean cachedIterator) {
    super(width, height, imageType);
    setVecs(cachedIterator);
  }

  public IterableBufferedImage(BufferedImage img) {
    this(img, false);
  }

  public IterableBufferedImage(BufferedImage img, boolean cachedIterator) {
    super(img.getColorModel(), img.copyData(null), img.isAlphaPremultiplied(), null);
    setVecs(cachedIterator);
  }

  private void setVecs(boolean cachedIterator) {
    if (cachedIterator) {
      vecs = new ArrayList<VecInt2D>();
      for (int y = 0; y < getHeight(); y++) {
        for (int x = 0; x < getWidth(); x++) {
          vecs.add(new VecInt2D(x, y));
        }
      }
    }
  }

  @Override
  public Iterator<VecInt2D> iterator() {
    if (vecs == null) {
      return new BufferedImageIterator();
    } else {
      return new CachedBufferedImageIterator();
    }
  }

  class BufferedImageIterator implements Iterator<VecInt2D> {

    private int x;
    private int y;

    BufferedImageIterator() {
      x = 0;
      y = 0;
    }

    @Override
    public boolean hasNext() {
      return (x < getWidth()) && (y < getHeight());
    }

    @Override
    public VecInt2D next() {
      if (hasNext()) {
        VecInt2D next = new VecInt2D(x, y);
        if (x < (getWidth() - 1)) {
          x++;
        } else {
          x = 0;
          y++;
        }
        return next;
      } else {
        throw new NoSuchElementException();
      }
    }

    @Override
    public void remove() {
      throw new UnsupportedOperationException();
    }

  }

  class CachedBufferedImageIterator implements Iterator<VecInt2D> {

    private int i;

    public CachedBufferedImageIterator() {
      i = 0;
    }

    @Override
    public boolean hasNext() {
      return i < vecs.size();
    }

    @Override
    public VecInt2D next() {
      if (hasNext()) {
        return vecs.get(i++);
      } else {
        throw new NoSuchElementException();
      }
    }

    @Override
    public void remove() {
      throw new UnsupportedOperationException();
    }

  }

}